import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import store from './store'
import router from './router'
import {action} from "./store/actions";
import {getter} from "./store/getters";
Vue.config.productionTip = false
Vue.use(Vuex)

Vue.filter('humanFileSize', function(size) {
  if (size) {
    const i = Math.floor( Math.log(size) / Math.log(1024) );
    return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
  }
  return size
});

new Vue({
  vuetify,
  store,
  router,
  render: h => h(App),
  async mounted() {
    if (!this.$store.getters[getter.GET_USER]) {
      await Promise.all([
        this.$store.dispatch(action.FETCH_USER),
        this.$store.dispatch(action.FETCH_GUILDS)
      ]);
    }
  }
}).$mount('#app')
