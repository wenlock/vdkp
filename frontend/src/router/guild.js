import GuildHome from "../views/GuildHome";
import GuildLogs from "../views/GuildLogs";
import GuildImport from "../views/GuildImport";
import GuildStandings from "../views/GuildStandings";

export default [
    {
        name: "GuildHome",
        path: '',
        component: GuildHome
    },
    {
        name: "GuildLogs",
        path: 'logs',
        component: GuildLogs
    },
    {
        name: "GuildImport",
        path: 'import',
        component: GuildImport
    },
    {
        name: "GuildStandings",
        path: 'standings',
        component: GuildStandings
    }
]
