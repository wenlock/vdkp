import Vue from 'vue'
import Router from 'vue-router'
import PageNotFound from "../views/PageNotFound";
import HelloWorld from "../views/HelloWorld";
import Guild from "../views/Guild";
import GuildChildren from "./guild";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'HelloWorld',
            component: HelloWorld
        },
        {
            path: '/guild/:guildId',
            component: Guild,
            children: GuildChildren
        },
        {path: "*", component: PageNotFound}
    ]
})
