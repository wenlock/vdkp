export const mutation = {
    CORS_REC: "CORS_REC",
    GUILDS_RECEIVED: "GUILD_RECEIVED",
    RAIDLOGS_RECEIVED: "RAIDLOGS_RECEIVED",
    REQUEST_FINISHED: "REQUEST_FINISHED",
    REQUEST_STARTED: "REQUEST_STARTED",
    SET_SELECTED_GUILD_ID: "SET_SELECTED_GUILD_ID",
    STANDINGS_RECEIVED: "STANDINGS_RECEIVED",
    USER_RECEIVED: "USER_RECEIVED",
}
export const mutations = {
    [mutation.USER_RECEIVED]: (state, payload) => {
        state.user = payload;
    },
    [mutation.GUILDS_RECEIVED]: (state, payload) => {
        state.guilds = payload;
    },
    [mutation.REQUEST_STARTED]: (state) => {
        state.onGoingRequests++;
    },
    [mutation.REQUEST_FINISHED]: (state) => {
        state.onGoingRequests--;
    },
    [mutation.SET_SELECTED_GUILD_ID]: (state, payload) => {
        state.selectedGuildId = payload;
    },
    [mutation.CORS_REC]: (state, payload) => {
        state.corsPayload = payload;
    },
    [mutation.RAIDLOGS_RECEIVED]: (state, payload) => {
        state.raidLogs = payload;
    },
    [mutation.STANDINGS_RECEIVED]: (state, payload) => {
        state.standings = payload;
    },
};
