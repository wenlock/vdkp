export const getter = {
    GET_GUILD: "getGuild",
    GET_GUILDS: "guilds",
    GET_RAIDLOGS: "raidLogs",
    GET_SELECTED_GUILD: "selectedGuild",
    GET_SELECTED_GUILD_ID: "selectedGuildId",
    GET_STANDINGS: "standings",
    GET_USER: "user",
    IS_LOADING: "isLoading",
}
export const getters = {
    [getter.GET_USER]: (state) => {
        return state.user
    },
    [getter.GET_GUILDS]: (state) => {
        return state.guilds
    },
    [getter.GET_STANDINGS]: (state) => {
        return state.standings
    },
    [getter.GET_GUILD]: (state) => (guildId) => {
        return state.guilds.find(o => o.id === guildId)
    },
    [getter.GET_SELECTED_GUILD_ID]: (state) => {
        return state.selectedGuildId
    },
    [getter.GET_SELECTED_GUILD]: (state) => {
        return state.guilds.find(o => o.id === state.selectedGuildId)
    },
    [getter.GET_RAIDLOGS]: (state) => {
        return state.raidLogs
    },
    [getter.IS_LOADING]: (state) => {
        return (state.onGoingRequests > 0)
    }
}
