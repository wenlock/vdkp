import Vue from 'vue'
import Vuex from 'vuex'
import {actions} from './actions'
import {getters} from './getters'
import {mutations} from './mutations'

Vue.use(Vuex);


export default new Vuex.Store({
    state: {
        corsPayload: null,
        guilds: [],
        menuVisible: false,
        onGoingRequests: 0,
        raidLogs: [],
        selectedGuildId: null,
        standings: [],
        user: null,
    },
    actions,
    getters,
    mutations,
    modules: {}
})
