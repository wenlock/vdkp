import axios from 'axios'
import {mutation} from "./mutations";

axios.defaults.headers.common['x-requested-with'] = "axios";

export const action = {
    FETCH_CORS: "FETCH_CORS",
    FETCH_GUILDS: "FETCH_GUILDS",
    FETCH_RAIDLOGS: "FETCH_RAIDLOGS",
    FETCH_STANDINGS:"FETCH_STANDINGS",
    FETCH_USER: "FETCH_USER",
    SAVE_RAIDLOG: "SAVE_RAIDLOG",
};
const fetchResource = (resource, commit, mutationName) => {
    commit(mutation.REQUEST_STARTED);
    return axios.get(resource).then(function (result) {
        if (result && result.data) {
            commit(mutationName, result.data)
        }
    }).finally(() => {
        commit(mutation.REQUEST_FINISHED);
    })
};
export const actions = {

    [action.FETCH_GUILDS]({commit}) {
        return fetchResource("/api/guilds", commit, mutation.GUILDS_RECEIVED)
    },

    [action.FETCH_USER]({commit}) {
        return fetchResource("/api/user", commit, mutation.USER_RECEIVED)
    },
    [action.SAVE_RAIDLOG]({commit, state}, payload) {
        commit(mutation.REQUEST_STARTED);
        const config = {
            headers: {'Content-Type': 'text/xml'}
        };
        const guildId = state.selectedGuildId;
        const resource = "/api/guild/{guildId}/raidlogs".replace("{guildId}", guildId);
        return axios.post(resource, payload, config).then(function (result) {
            commit(mutation.RAIDLOGS_RECEIVED, result.data)
        }).finally(() => {
            commit(mutation.REQUEST_FINISHED);
        })
    },
    [action.FETCH_CORS]({commit}) {
        const url = "https://storage.googleapis.com/vkp-ninja/cors-test.json";
        return fetchResource(url, commit, mutation.CORS_REC)
    },
    [action.FETCH_STANDINGS]({commit}, guildId) {
        const url = "https://storage.googleapis.com/vendetta.appspot.com/guild/{guildId}/results/standings.json".replace("{guildId}", guildId);
        return fetchResource(url, commit, mutation.STANDINGS_RECEIVED)
    },
    [action.FETCH_RAIDLOGS]({commit}, guildId) {
        const resource = "/api/guild/{guildId}/raidlogs".replace("{guildId}", guildId);
        return fetchResource(resource, commit, mutation.RAIDLOGS_RECEIVED)
    },
};
// https://storage.googleapis.com/vkp-ninja/cors-test.json