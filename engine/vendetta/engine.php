<?php

use vDKP\Awards\BossLootPrice;
use vDKP\Awards\FlatBonus;
use vDKP\Awards\MedianPlayerDkp;
use vDKP\Awards\PercentOfBossDkp;
use vDKP\Awards\PercentOfGuildDkp;
use vDKP\Awards\PercentOfPlayerDkp;
use vDKP\RuleEngine;
use vDKP\Rules\BossKillBonus;
use vDKP\Rules\CrowdedBonus;
use vDKP\Rules\DeductLootMinimumBid;
use vDKP\Rules\FirstKillBonus;
use vDKP\Rules\NonProgressKickback;
use vDKP\Rules\SignonFee;
use vDKP\Rules\SpecialLootBonus;
use vDKP\Rules\SpeedBonus;
use vDKP\Rules\WeeklyDecay;
use vDKP\Utils;

require_once dirname( __FILE__ ) . "/../../vendor/autoload.php";
$speedRules = require_once "rules" . DIRECTORY_SEPARATOR . "speedrules.php";
$engine     = new RuleEngine();
$engine->addRule( new SignonFee( new FlatBonus( 100 ) ) );
$engine->addRule( new WeeklyDecay( new PercentOfPlayerDkp( - 0.15 ) ) );
$engine->addRule( new DeductLootMinimumBid( new BossLootPrice() ) );
$engine->addRule( new CrowdedBonus( new PercentOfBossDkp( 0.2 ) ) );
$engine->addRule( new BossKillBonus( new PercentOfBossDkp( 1.4 ) ) );
$engine->addRule( new FirstKillBonus( new PercentOfGuildDkp( 0.002 ) ) );


foreach ( $speedRules as $c ) {
	$config = [
		SpeedBonus::CONF_NAME      => $c[0],
		SpeedBonus::CONF_BOSSES    => $c[1],
		SpeedBonus::CONF_TIMEFRAME => $c[2],
		SpeedBonus::CONF_AWARD_ALL => $c[3],
	];
	$award  = new PercentOfBossDkp( $c[4] );
	$engine->addRule( new SpeedBonus( $award, $config ) );
}
$specialLootRules = [
	[
		"LegendaryBonus",
		0.1,
		[
			"Eye of Sulfuras",
			"Bindings of the Windseeker"
		],
	],
	[
		"LootCouncilBonus",
		0.05,
		[
			"Eye of Sulfuras",
			"Bindings of the Windseeker",
			"Neltharion's Tear",
			"Styleen's Impeding Scarab",
			"Mark of C'Thun",
			"Breastplate of Wrath",
			"Dragonbreath Hand Cannon",
			"Ring of Emperor Vek'lor",
			"Cryptfiend Silk Cloak"
		],
	],
];
foreach ( $specialLootRules as $c ) {
	$config = [
		SpecialLootBonus::CONF_NAME      => $c[0],
		SpecialLootBonus::CONF_ITEMNAMES => $c[2]
	];
	$award  = new MedianPlayerDkp( $c[1] );
	$engine->addRule( new SpecialLootBonus( $award, $config ) );
}

$progressItems = Utils::fileToArray( dirname( __FILE__ ) . "/lists/pveitems.txt" );
$award         = new FlatBonus( 25 );
$engine->addRule( new NonProgressKickback( $award, [ NonProgressKickback::CONF_ITEMNAMES => $progressItems ] ) );

return $engine;