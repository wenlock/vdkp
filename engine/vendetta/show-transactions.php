<?php
$state = require_once "events.php";

$lines = [];
foreach ( $state->transactions as $transaction ) {
	if ( $transaction->getPlayerName() === "Wenlock" ) {
		$lines[] = $transaction->getTitle();
	}
}
sort( $lines );
echo( implode( PHP_EOL, $lines ) );