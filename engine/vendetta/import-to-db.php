<?php

use VkpNinja\Db;
use VkpNinja\Util;

require_once dirname( __FILE__ ) . "/../../vendor/autoload.php";
if (Util::isDevelopment()) {
    call_user_func_array(['\GaeFlow\Router', 'loadDevEnv'], []);
}

$events_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . "events";
$eventFiles = [];
foreach (scandir($events_dir) as $filename) {
    $path = $events_dir . DIRECTORY_SEPARATOR . $filename;
    $ext = pathinfo($path, PATHINFO_EXTENSION);
    if ($ext === "xml") {
        $eventFiles[] = $path;
    }
}

foreach ($eventFiles as $file){
    $d = Db::saveDkpString(file_get_contents($file));
    print_r($d);
}
