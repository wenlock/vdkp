<?php

use vDKP\Utils;

$events_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . "events";
$eventFiles = [];
foreach (scandir($events_dir) as $filename) {
    $path = $events_dir . DIRECTORY_SEPARATOR . $filename;
    $ext = pathinfo($path, PATHINFO_EXTENSION);
    if ($ext === "xml") {
        $eventFiles[] = $path;
    }
}
$engine = require_once "engine2.php";
return Utils::loadDkpFiles($engine, $eventFiles);