<?php

use vDKP\Utils;

$state = require_once "events.php";

$transactions = $state->getTransactionsAsArray();
$path = Utils::path( "tmp", "transactions2.csv" );
file_put_contents( $path, Utils::toCsv( $transactions ) );
//file_put_contents( Utils::path( "tmp", "transactions2.json" ), Utils::toJson( $transactions ) );
echo("Data was exported to " . $path);