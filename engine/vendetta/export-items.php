<?php

use vDKP\Models\State;
use vDKP\Utils;

$state = require_once "events.php";
/* @var State $state */
$items = $state;
file_put_contents( Utils::path( "tmp", "items.csv" ), Utils::toCsv( $items ) );
file_put_contents( Utils::path( "tmp", "items.json" ), Utils::toJson( $items ) );