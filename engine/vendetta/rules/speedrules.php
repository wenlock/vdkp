<?php
return [
	[
		"BlackwingDown",
		[ "Razorgore", "Chromaggus" ],
		90,
		false,
		0.5
	],
	[
		"TheFamilicide",
		[ "Nefarian", "Onyxia" ],
		30,
		false,
		0.5,
	],
	[
		"FlamewalkingDead",
		[ "Lucifron", "Gehennas", "Shazzrah", "Sulfuron Harbinger" ],
		45,
		true,
		1.4,

	],
	[
		"TheAxeman",
		[ "Ragnaros ", "Lucifron" ],
		60,
		false,
		1
	],
	[
		"FireFighter",
		[ "Raggy", "Onyxia" ],
		30,
		false,
		0.5,

	],
	[
		"ThreeDrakes",
		[ "Firemaw", "Ebonroc", "Flamegore" ],
		45,
		true,
		1
	],
];