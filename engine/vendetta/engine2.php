<?php

use vDKP\Awards\BossLootPrice;
use vDKP\Awards\FixedBossValue;
use vDKP\Awards\FlatBonus;
use vDKP\Awards\PercentOfBossDkp;
use vDKP\Awards\PercentOfPlayerDkp;
use vDKP\Models\RaidBreak;
use vDKP\RuleEngine;
use vDKP\Rules\BossKillBonus;
use vDKP\Rules\DeductLoot;
use vDKP\Rules\FirstKillBonus;
use vDKP\Rules\MultiCharTransfer;
use vDKP\Rules\SignonFee;
use vDKP\Rules\SpeedBonus;
use vDKP\Rules\WeeklyDecay;
use vDKP\Utils;

require_once dirname(__FILE__) . "/../../vendor/autoload.php";
$speedRules = require_once "rules" . DIRECTORY_SEPARATOR . "speedrules.php";
$bossRewards = Utils::csvToAssocArray(dirname(__FILE__) . '/lists/bossrewards.csv');
$engine = new RuleEngine();
$engine->addRule(new SignonFee(new FlatBonus(100)));
$engine->addRule(new DeductLoot(new BossLootPrice()));
$engine->addRule(new BossKillBonus(new FixedBossValue(1, $bossRewards)));
$engine->addRule(new FirstKillBonus(new FixedBossValue(1, $bossRewards)));
$engine->addRule(new WeeklyDecay(new PercentOfPlayerDkp(-0.15), [
    WeeklyDecay::CONF_BREAKS => [
        RaidBreak::make("2019-12-23", "2020-01-06", "Juleferie"),
        RaidBreak::make("2020-01-23", "2020-02-08", "Ferie", "Tangkvast")
    ]
]));
$engine->addRule(new MultiCharTransfer(null, [
    MultiCharTransfer::CONF_CHARS => [
        "Arja,Sipzoe",
        "Herkulez,Molfrid",
        "Knatten,Gammelmorfar",
        "Kolli,Kollipala,Kollimage",
        "Lorby,Atarie,Ferocia",
        "Mobytank,Mobya",
        "Pexlock,Pexdruid,Pexbox",
        "Superdwarf,Supergnome",
        "Woorak,Sedona",
        "Wryhnz,Xmage,Wrynzo",
    ],
    MultiCharTransfer::CONF_ON_RULES => [
        BossKillBonus::class,
        FirstKillBonus::class,
        SpeedBonus::class,
    ]
]));

foreach ($speedRules as $c) {
    $config = [
        SpeedBonus::CONF_NAME => $c[0],
        SpeedBonus::CONF_BOSSES => $c[1],
        SpeedBonus::CONF_TIMEFRAME => $c[2],
        SpeedBonus::CONF_AWARD_ALL => $c[3],
    ];
    $award = new PercentOfBossDkp($c[4]);
    $engine->addRule(new SpeedBonus($award, $config));
}

return $engine;