<?php


namespace vDKP;


use vDKP\Events\BossKillEvent;
use vDKP\Events\LootEvent;
use vDKP\Events\WeekStartEvent;
use vDKP\Models\Player;
use vDKP\Models\Transaction;

class Create {

	/**
	 * @param $playerName
	 * @param $amount
	 *
	 * @return Transaction
	 */
	static function transaction( $playerName, $amount ) {
		$transaction         = new Transaction( new Player( $playerName ), new WeekStartEvent( self::getNextTime() ) );
		$transaction->setAmount($amount);

		return $transaction;
	}

	static function getNextTime() {
		static $offset = 0;
		$date        = new \DateTime();
		$modificator = "+ " . $offset . " hours";
		$date->modify( $modificator );
		$offset++;
		return $date;
	}

	/**
	 * @param $bossName
	 * @param $price
	 *
	 * @return LootEvent
	 * @throws \Exception
	 */
	static function lootEvent( $bossName, $price ) {
		$event           = new LootEvent( self::getNextTime() );
		$event->bossName = $bossName;
		$event->value    = $price;

		return $event;
	}

	/**
	 * @param $bossName
	 *
	 * @return LootEvent
	 * @throws \Exception
	 */
	static function bossKillEvent( $bossName ) {
		$event           = new BossKillEvent( self::getNextTime() );
		$event->bossName = $bossName;

		return $event;
	}
}