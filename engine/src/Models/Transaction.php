<?php

namespace vDKP\Models;


use DateTime;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Utils;

class Transaction {
	var $time;
	var $ruleName;
	var $player;
	var $amount;
	var $title;
	var $eventText;
    var $rule;

    /**
     * @return RuleInterface
     */
    public function getRule(): RuleInterface {
        return $this->rule;
    }

    /**
     * @param RuleInterface $rule
     */
    public function setRule(RuleInterface $rule): void {
        $this->rule = $rule;
    }

	public function __construct(
		Player $player,
		EventInterface $event,
		RuleInterface $rule = null
	) {
		$this->player    = $player;
		$this->time      = $event->getEventTime();
		$this->eventText = $event->getEventText();
		if ( ! is_null( $rule ) ) {
			$this->ruleName = $rule->getName();
			$this->rule = $rule;
		}
	}

	public function setAmount( $amount ) {
		$this->amount = (int) Utils::roundUp( $amount );
	}

	public function getTitle() {
		$amount = abs( $this->amount );
		if ( $this->amount > 0 ) {
			$action = "awarded $amount dkp";
		} else {
			$action = "deducted $amount dkp";
		}

		return $this->time->format( "c" ) . ": " . $this->getPlayerName() .
		       " was $action " . $this->ruleName .
		       " when " . $this->eventText;
	}

	public function getEventText() {
		return $this->eventText;
	}

	public function getPlayerName() {
		return $this->player->name;
	}

	/**
	 * @return DateTime
	 */
	public function getTransactionTime() {
		return $this->time;
	}

	/**
	 * @return integer
	 */
	public function getAmount() {
		return $this->amount;
	}

	/**
	 * @return integer
	 */
	public function getRuleName() {
		return $this->ruleName;
	}
}