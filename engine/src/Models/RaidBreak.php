<?php

namespace vDKP\Models;

use DateTime;

class RaidBreak {

    const ALL_PLAYERS = "*";
    var $start;
    var $end;
    var $name;
    var $who;

    public function __construct(\DateTime $start, \DateTime $end, $name = "holiday", $who = self::ALL_PLAYERS) {
        $this->start = $start;
        $this->end = $end;
        $this->name = $name;
        $this->who = $who;
    }

    public function isForAllPlayers() {
        return $this->who === self::ALL_PLAYERS;
    }

    public function getForWho() {
        if ($this->isForAllPlayers()) {
            return [];
        } elseif (is_string($this->who)) {
            return [$this->who];
        } elseif (is_array($this->who)) {
            return $this->who;
        } else {
            return [];
        }
    }

    /**
     * @return \DateTime
     */
    public function getStart(): \DateTime {
        return $this->start;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return \DateTime
     */
    public function getEnd(): \DateTime {
        return $this->end;
    }

    /**
     * @param $startTimeStr
     * @param $endTimeStr
     * @param string $name
     * @param string $who
     * @return RaidBreak
     * @throws \Exception
     */
    static function make($startTimeStr, $endTimeStr, $name, $who = self::ALL_PLAYERS) {
        return new RaidBreak(
            new DateTime($startTimeStr),
            new DateTime($endTimeStr),
            $name,
            $who,
        );
    }
}