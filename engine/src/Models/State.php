<?php

namespace vDKP\Models;

use DateTime;
use vDKP\Abstracts\AbstractEvent;
use vDKP\Events\BossKillEvent;
use vDKP\Events\LootEvent;
use vDKP\Events\WeekStartEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Utils;

/**
 * Class State
 *
 * No logic, just helpers to get information about he current state.
 */
class State {

    /**
     * @var AbstractEvent[]
     */
    var $events = [];

    /**
     * @var Transaction[]
     */
    var $transactions = [];

    private $_cache = [];

    function getPlayerDKP(Player $player) {
        if (array_key_exists($player->name, $this->getAllDKPByPlayer())) {
            return $this->getAllDKPByPlayer()[$player->name];
        }

        return 0;
    }

    /**
     * @return Player[]
     */
    function getPlayers() {
        $players = [];
        foreach (array_keys($this->getAllDKPByPlayer()) as $playerName) {
            $players[] = new Player($playerName);
        }

        return $players;
    }

    function getMedianDkp($excludePlayersBelow = 0) {
        $numbers = [];
        foreach (array_values($this->getAllDKPByPlayer()) as $points) {
            if ($points > $excludePlayersBelow) {
                $numbers[] = $points;
            }
        }
        sort($numbers);
        $index = floor(sizeof($numbers) / 2);
        if ($numbers[$index]) {
            return $numbers[$index];
        } else {
            return 0;
        }
    }

    /**
     * @return Player[]
     */
    function getAllPlayers() {
        if (!array_key_exists(__FUNCTION__, $this->_cache)) {
            $players = [];
            foreach ($this->events as $event) {
                foreach ($event->getPlayers() as $player) {
                    $players[$player->name] = $player;
                }
            }
            $this->_cache[__FUNCTION__] = $players;
        }

        return $this->_cache[__FUNCTION__];
    }

    function getAllDKPByPlayer() {
        if (!array_key_exists(__FUNCTION__, $this->_cache)) {
            $players = [];
            foreach ($this->transactions as $transaction) {
                if (!array_key_exists($transaction->getPlayerName(), $players)) {
                    $players[$transaction->getPlayerName()] = [];
                }
                $players[$transaction->getPlayerName()][] = $transaction->amount;
            }
            foreach ($players as $playerName => $arrayOfAmounts) {
                $players[$playerName] = array_sum($arrayOfAmounts);
            }
            $this->_cache[__FUNCTION__] = $players;
        }

        return $this->_cache[__FUNCTION__];
    }

    /**
     * Tar inn en event og de transkasjonene som følger.
     * Clears memorization.
     *
     * @param EventInterface $event
     * @param Transaction[] $transactions
     */
    function mutate(EventInterface $event, $transactions) {
        $this->events[] = $event;
        Utils::eventSort($this->events);
        foreach ($transactions as $transaction) {
            $this->transactions[] = $transaction;
        }
        // Reset Cache
        $this->_cache = [];
    }

    function getAllDkpByLootItem() {
        if (!array_key_exists(__FUNCTION__, $this->_cache)) {
            $lootItems = [];
            $lootValues = [];
            foreach ($this->events as $event) {
                if (is_a($event, LootEvent::class)) {
                    /** @var $event LootEvent */
                    if (!array_key_exists($event->bossName, $lootValues)) {
                        $lootValues[$event->bossName] = [];
                    }
                    $lootValues[$event->bossName][] = $event->value;
                }
                foreach ($lootValues as $bossName => $lootValues) {
                    $bosses[$bossName] = array_sum($lootValues) / $bossKills[$bossName];
                }
            }

            return $this->_cache[__FUNCTION__];
        }
    }

    /**
     * @param $bossName
     *
     * @return BossKillEvent[]
     */
    function getBossKillsByName($bossName) {
        $events = [];
        foreach ($this->events as $event) {
            /** @var $event BossKillEvent */
            if (is_a($event, BossKillEvent::class)
                && $event->bossName === $bossName) {
                $events[] = $event;
            }
        }

        return $events;
    }

    /**
     * @param $bossName
     *
     * @return BossKillEvent
     */
    function getLastBossKillByName($bossName) {
        $bossKillsByName = array_values($this->getBossKillsByName($bossName));

        return end($bossKillsByName);
    }

    /**
     * @return LootEvent[]
     */
    function getLootEvents() {
        $events = [];
        foreach ($this->events as $event) {
            /** @var $event LootEvent */
            if (is_a($event, LootEvent::class)) {
                $events[] = $event;
            }
        }

        return $events;
    }

    function getHigestLootValue() {
        $values = [];
        foreach ($this->getLootEvents() as $event) {
            $values[] = $event->value;
        }

        return max($values);
    }

    /**
     * @param $itemName
     *
     * @return LootEvent[]
     */
    function getLootEventsByItem($itemName) {
        $events = [];
        foreach ($this->getLootEvents() as $event) {
            if ($event->itemName === $itemName) {
                $events[] = $event;
            }
        }

        return $events;
    }

    function getAvgLootValueByItem($itemName) {
        $events = $this->getLootEventsByItem($itemName);
        $itemValues = [];
        foreach ($events as $event) {
            $itemValues[] = (int)$event->value;
        }
        if (count($itemValues)) {
            $itemValues = array_values($itemValues);
            $average = array_sum($itemValues) / count($itemValues);

            return $average;
        } else {
            return 0;
        }

    }

    /**
     * @param Player $player
     *
     * @return LootEvent[]
     */
    function getLootEventsByPlayer(Player $player) {
        $events = [];
        foreach ($this->events as $event) {
            /** @var $event LootEvent */
            if (is_a($event, LootEvent::class)
                && $player == $event->getBuyer()) {
                $events[] = $event;
            }
        }

        return $events;
    }

    function getMaxItemPrice($itemName) {
        $previousLootEvents = $this->getLootEventsByItem($itemName);
        $prices = [];
        foreach ($previousLootEvents as $lootEvents) {
            $prices[] = $lootEvents->value;
        }
        if (count($prices)) {
            return max($prices);
        } else {
            return 0;
        }
    }

    /**
     * @return EventInterface
     */
    function getLastWeekEvent() {
        $events = array_filter($this->events, function (EventInterface $event) {
            return is_a($event, WeekStartEvent::class);
        });

        return end($events);
    }

    function getAverageBossLootValues(DateTime $before = null) {
        if (!array_key_exists(__FUNCTION__, $this->_cache)) {
            $lastWeekEvent = $this->getLastWeekEvent();
            $bosses = [];
            $bossKills = [];
            $lootValues = [];
            $events = array_filter($this->events, function (EventInterface $event) use ($lastWeekEvent) {
                return $lastWeekEvent ?
                    ($event->getEventTime()->getTimestamp() < $lastWeekEvent->getEventTime()->getTimestamp())
                    : true;
            });
            foreach ($events as $event) {
                if (isset($event->bossName) && !array_key_exists($event->bossName, $bosses)) {
                    $lootValues[$event->bossName] = [];
                    $bossKills[$event->bossName] = 0;
                    $bosses[$event->bossName] = 0;
                }
                if (is_a($event, LootEvent::class)) {
                    /** @var $event LootEvent */
                    $lootValues[$event->bossName][] = $event->value;
                }
                if (is_a($event, BossKillEvent::class)) {
                    /** @var $event BossKillEvent */
                    $bossKills[$event->bossName]++;
                }
                foreach ($lootValues as $bossName => $lootValue) {
                    $bossKillDivident = $bossKills[$bossName] === 0 ? 1 : $bossKills[$bossName];
                    $bosses[$bossName] = array_sum($lootValue) / $bossKillDivident;
                }
            }

            $this->_cache[__FUNCTION__] = $bosses;
        }

        return $this->_cache[__FUNCTION__];
    }

    function getBossKillDkp($bossName) {
        $bossLootValues = $this->getAverageBossLootValues();

        return max([
            array_key_exists($bossName, $bossLootValues) ? $bossLootValues[$bossName] : 0,
            $this->getMinimumBossKillDkp() * 40
        ]);
    }

    function getGuildDkp() {
        return array_sum(array_values($this->getAllDKPByPlayer()));
    }

    function getLastBossKills() {
        $bosses = [];
        foreach ($this->events as $event) {
            if (is_a($event, BossKillEvent::class)) {
                $bosses[$event->bossName] = $event->eventTime;
            }
        }

        return $bosses;
    }

    /**
     * @return Transaction[]
     */
    function getTransactions() {
        $transactions = array_values($this->transactions);
        Utils::transactionSort($transactions);

        return $transactions;
    }

    /**
     * @param $playerName
     * @param DateTime $startTime
     * @param DateTime $untillTime
     * @return Transaction[]
     */
    function getTransactionsByPlayer($playerName, DateTime $startTime, DateTime $untillTime) {
        return array_filter($this->getTransactions(), function (Transaction $transaction) use ($playerName, $startTime, $untillTime) {
            return (
                $transaction->getTransactionTime() >= $startTime &&
                $transaction->getTransactionTime() <= $untillTime &&
                $transaction->getPlayerName() === $playerName
            );
        });
    }

    function getLootAsArray() {
        $output = [];
        foreach ($this->getLootEvents() as $event) {
            $output[] = [
                "time" => $event->getEventTime()->format("c"),
                "player" => $event->getBuyer()->name,
                "when" => $event->getEventText(),
                "item" => $event->itemName,
                "value" => $event->value,
                "boss" => $event->bossName,
            ];
        }

        return $output;
    }

    function getTransactionsAsArray() {
        $output = [];
        foreach ($this->getTransactions() as $transaction) {
            $output[] = [
                "time" => $transaction->getTransactionTime()->format("c"),
                "player" => $transaction->getPlayerName(),
                "rule" => $transaction->getRuleName(),
                "when" => $transaction->getEventText(),
                "amount" => $transaction->getAmount(),
            ];
        }

        return $output;
    }

    function getMinimumBossKillDkp() {
        return 2;
    }

    /**
     * @param DateTime $startTime
     * @param DateTime $untillTime
     * @param array $ruleClasses
     * @return Transaction[]
     */
    function getTransactionsBetween(DateTime $startTime, DateTime $untillTime, array $ruleClasses) {
        return array_filter($this->getTransactions(), function (Transaction $transaction) use ($startTime, $untillTime,$ruleClasses) {
            $ruleClass = get_class($transaction->getRule());
            return (
                $transaction->getTransactionTime() >= $startTime &&
                $transaction->getTransactionTime() <= $untillTime &&
                in_array($ruleClass, $ruleClasses)
            );
        });
    }
}
