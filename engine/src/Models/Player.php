<?php


namespace vDKP\Models;


class Player {
	var $name;
	var $class;
	var $comment;

	public function __construct( $name, $class = null, $comment = null ) {
		$this->name    = $name;
		$this->class   = $class;
		$this->comment = $comment;
	}
}