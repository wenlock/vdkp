<?php


namespace vDKP\Models;


use DateTime;

class Attendee {
	var $startTime;


	var $endTime;
	var $player;

	public function __construct( Player $player ) {
		$this->player = $player;
	}

	/**
	 * @return DateTime
	 */
	public function getStartTime() {
		return $this->startTime;
	}

	/**
	 * @param   DateTime  $startTime
	 */
	public function setStartTime( DateTime $startTime ): void {
		$this->startTime = $startTime;
	}

	/**
	 * @return DateTime
	 */
	public function getEndTime() {
		return $this->endTime;
	}

	/**
	 * @param   DateTime  $endTime
	 */
	public function setEndTime( DateTime $endTime ): void {
		$this->endTime = $endTime;
	}

	/**
	 * @return Player
	 */
	public function getPlayer(): Player {
		return $this->player;
	}

	/**
	 * @param   Player  $player
	 */
	public function setPlayer( Player $player ): void {
		$this->player = $player;
	}

}