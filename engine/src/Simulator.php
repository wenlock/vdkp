<?php


namespace vDKP;


use Faker\Factory;
use vDKP\Models\Player;

class Simulator {

	var $players = [];
	var $idealComposition = [];
	var $faker;

	public function __construct() {
		$this->idealComposition = [
			PlayableClass::WARRIOR => 8,
			PlayableClass::ROGUE   => 6,
			PlayableClass::MAGE    => 6,
			PlayableClass::PRIEST  => 5,
			PlayableClass::WARLOCK => 5,
			PlayableClass::HUNTER  => 3,
			PlayableClass::DRUID   => 3,
			PlayableClass::PALADIN => 4
		];
		$this->faker            = Factory::create();
	}

	public function addRandomPlayers( $numberOfPlayers, $comment = null ) {
		for ( $i = 1; $i <= $numberOfPlayers; $i ++ ) {
			$name                   = $this->getRandomPlayerName();
			$this->players[ $name ] = new Player( $name, $this->getRandomClass(), $comment );
		}
	}

	public function getRandomPlayerName() {
		static $names = [];
		$name = $this->faker->firstName;
		if ( in_array( $name, $names ) ) {
			return $this->getRandomPlayerName();
		}
		array_push( $names, $name );

		return $name;
	}

	/**
	 * @param $playableClass
	 *
	 * @return Player
	 */
	public function getRandomPlayer( $playableClass = null ) {
		if ( is_null( $playableClass ) ) {
			$playableClass = $this->getRandomClass();
		}

		return new Player( $this->getRandomPlayerName(), $playableClass );
	}

	public function getRandomClass() {
		$idealRaid = [];
		foreach ( $this->idealComposition as $class => $numberOfPlayers ) {
			for ( $i = 1; $i <= $numberOfPlayers; $i ++ ) {
				$idealRaid[] = $class;
			}
		}

		return $idealRaid[ array_rand( $idealRaid ) ];
	}

	public function getRandomName() {

	}
}