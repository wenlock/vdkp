<?php


namespace vDKP\Awards;


use vDKP\Abstracts\AbstractAward;
use vDKP\Interfaces\AwardInterface;
use vDKP\Interfaces\EventInterface;
use vDKP\Models\Player;
use vDKP\Models\State;

class PercentOfPlayerDkp extends AbstractAward implements AwardInterface {
	public function __invoke( Player $player, EventInterface $event, State $state ) {
		$playerDkp = $state->getPlayerDKP( $player );
		if ( $playerDkp > 0 ) {
			return $state->getPlayerDKP( $player ) * $this->modifier;
		} else {
			return 0;
		}
	}
}