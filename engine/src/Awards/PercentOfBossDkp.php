<?php


namespace vDKP\Awards;


use vDKP\Abstracts\AbstractAward;
use vDKP\Interfaces\AwardInterface;
use vDKP\Interfaces\EventInterface;
use vDKP\Models\BossKillEvent;
use vDKP\Models\Player;
use vDKP\Models\State;

class PercentOfBossDkp extends AbstractAward implements AwardInterface {

	/**
	 * @var EventInterface
	 */
	var $event;

	public function __invoke( Player $player, EventInterface $event, State $state ) {
		return ($state->getBossKillDkp( $event->bossName ) / 40) * $this->modifier;
	}
}