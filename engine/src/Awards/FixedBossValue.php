<?php


namespace vDKP\Awards;


use vDKP\Abstracts\AbstractAward;
use vDKP\Events\BossKillEvent;
use vDKP\Interfaces\AwardInterface;
use vDKP\Interfaces\EventInterface;
use vDKP\Models\Player;
use vDKP\Models\State;

class FixedBossValue extends AbstractAward implements AwardInterface {
	public function __invoke( Player $player, EventInterface $event, State $state ) {
		/* @var $event BossKillEvent */
		if(array_key_exists($event->bossName,$this->fixedValues)){
			return $this->fixedValues[$event->bossName];
		} else {
			print_r("Warning: ".$event->bossName. " not found in list.");
			return 0;
		}
	}
}