<?php


namespace vDKP\Awards;


use vDKP\Abstracts\AbstractAward;
use vDKP\Events\BossKillEvent;
use vDKP\Interfaces\AwardInterface;
use vDKP\Interfaces\EventInterface;
use vDKP\Models\Player;
use vDKP\Models\State;

class BossLootPrice extends AbstractAward implements AwardInterface {
	public function __invoke( Player $player, EventInterface $event, State $state ) {
		/* @var BossKillEvent $event */
		$bossKillDkp = $state->getBossKillDkp( $event->bossName );
		return $bossKillDkp * $this->modifier;
	}
}