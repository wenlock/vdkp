<?php


namespace vDKP\Awards;


use vDKP\Abstracts\AbstractAward;
use vDKP\Interfaces\AwardInterface;
use vDKP\Interfaces\EventInterface;
use vDKP\Models\Player;
use vDKP\Models\State;

class PercentOfLootDkp extends AbstractAward implements AwardInterface {

	public function __invoke( Player $player, EventInterface $event, State $state ) {
		return $event->value * $this->modifier;
	}
}