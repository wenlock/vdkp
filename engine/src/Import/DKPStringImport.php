<?php

namespace vDKP\Import;

use DateTime;
use SimpleXMLElement;
use vDKP\Events\AdjustmentEvent;
use vDKP\Events\BossKillEvent;
use vDKP\Events\FreezeEvent;
use vDKP\Events\LootEvent;
use vDKP\Events\TransferEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Models\Attendee;
use vDKP\Models\Player;
use vDKP\Utils;

class DKPStringImport {

	/**
	 * @var EventInterface[]
	 */
	var $events = [];

	/**
	 * @var Player[]
	 */
	var $players = [];

	var $zones = [];

	/**
	 * @var Attendee[]
	 */
	var $attendance = [];

	public function __construct( $dkpString ) {
		$xml = simplexml_load_string( $dkpString );
		if ( $xml->raiddata && $xml->raiddata->members ) {
			$this->addMembers( $xml->raiddata->members );
		}
		if ( $xml->raiddata && $xml->raiddata->bosskills ) {
			$this->addBosskills( $xml->raiddata->bosskills );
		}
		if ( $xml->raiddata && $xml->raiddata->items ) {
			$this->addItems( $xml->raiddata->items );
		}
		if ( $xml->raiddata && $xml->raiddata->zones ) {
			$this->addZones( $xml->raiddata->zones );
		}
		if ( $xml->adjustments ) {
			$this->addAdjustments( $xml->adjustments );
		}
		if ( $xml->transfers ) {
			$this->addTransfers( $xml->transfers );
		}
		if ( $xml->freezes ) {
			$this->addFreezes( $xml->freezes );
		}
		Utils::eventSort( $this->events );
	}

	function addFreezes(SimpleXMLElement $freezes){
		foreach ( $freezes->children() as $freeze ) {
			$freezeTime  = Utils::fromTimestamp( $freeze->time );
			$freezeEvent= new FreezeEvent( $freezeTime );
			$freezeEvent->setValue( (int) $freeze->value );
			$freezeEvent->addPlayer( new Player( (string) $freeze->member ) );
			$this->addEvent( $freezeEvent );
		}
	}
	function addAdjustments( SimpleXMLElement $adjustments ) {
		foreach ( $adjustments->children() as $adjustment ) {
			$adjustmentTime  = Utils::fromTimestamp( $adjustment->time );
			$adjustmentEvent = new AdjustmentEvent( $adjustmentTime );
			$adjustmentEvent->setValue( (int) $adjustment->value );
			$adjustmentEvent->addPlayer( new Player( (string) $adjustment->member ) );
			$adjustmentEvent->setEventText( (string) $adjustment->text );
			$this->addEvent( $adjustmentEvent );
		}
	}

	function addTransfers( SimpleXMLElement $transfers ) {
		foreach ( $transfers->children() as $transfer ) {
			$transferTime  = Utils::fromTimestamp( $transfer->time );
			$transferEvent = new TransferEvent( $transferTime );
			if($transfer->startTime){
                $transferEvent->setStartTime(Utils::fromTimestamp($transfer->startTime));
            }
			$transferEvent->setFromPlayer( new Player( (string) $transfer->from ) );
			$transferEvent->setToPlayer( new Player( (string) $transfer->to ) );
			$this->addEvent( $transferEvent );
		}
	}

	function addZones( SimpleXMLElement $zones ) {
		foreach ( $zones->children() as $zone ) {
			$enterTime     = Utils::fromTimestamp( $zone->enter );
			$leaveTime     = Utils::fromTimestamp( $zone->leave );
			$this->zones[] = [
				"enter" => $enterTime,
				"leave" => $leaveTime,
				"name"  => (string) $zone->name,
			];
		}
	}

	function addItems( SimpleXMLElement $items ) {
		foreach ( $items->children() as $item ) {
			$eventTime           = Utils::fromTimestamp( $item->time );
			$playerName          = (string) $item->member;
			$lootEvent           = new LootEvent( $eventTime );
			$lootEvent->itemName = (string) $item->name;
			$lootEvent->value    = (int) $item->cost;
			$lootEvent->bossName = (string) $item->boss;
			if ( in_array( $playerName, [ "bank", "disenchanted" ] ) ) {
				$lootEvent->isGuildBank = true;
			} else {
				$lootEvent->setBuyer( new Player( $playerName ) );
			}
			$event = $this->addAttendance( $lootEvent );
			$this->addEvent( $event );
		}
	}

	function addBosskills( SimpleXMLElement $bosskills ) {
		foreach ( $bosskills->children() as $bosskill ) {
			$eventTime               = Utils::fromTimestamp( $bosskill->time );
			$bossKillEvent           = new BossKillEvent( $eventTime );
			$bossKillEvent->bossName = (string) $bosskill->name;
			$event                   = $this->addAttendance( $bossKillEvent );
			$this->addEvent( $event );
		}
	}

	function addMembers( SimpleXMLElement $members ) {
		foreach ( $members->children() as $member ) {
			$player          = new Player( (string) $member->name, (string) $member->class );
			$this->players[] = $player;
			$startTime       = null;
			$endTime         = null;
			foreach ( $member->times->children() as $time ) {
				$endTime = (int) $time;
				if ( is_null( $startTime ) ) {
					$startTime = (int) $time;
				}
			}
			$attendee = new Attendee( $player );
			$attendee->setStartTime( Utils::fromTimestamp( $startTime ) );
			$attendee->setEndTime( Utils::fromTimestamp( $endTime ) );
			$this->attendance[] = $attendee;
		}
	}

	public function addAttendance( EventInterface $event ) {
		foreach ( $this->attendance as $attendee ) {
			if ( ( $event->getEventTime() > $attendee->getStartTime() )
			     && ( $event->getEventTime() <= $attendee->getEndTime() ) ) {
				$event->addPlayer( $attendee->getPlayer() );
			}
		}

		return $event;
	}

	function addEvent( EventInterface $event ) {
		$this->events[] = $event;
	}

	/**
	 * @return BossKillEvent[]
	 */
	public function getOtherEvents() {
		return array_filter( $this->events, function ( EventInterface $event ) {
			return ! is_a( $event, LootEvent::class );
		} );
	}

	/**
	 * @return EventInterface[]
	 */
	public function getAllEvents() {
		return $this->events;
	}

	/**
	 * @return LootEvent[]
	 */
	public function getLootEvents() {
		return array_filter( $this->events, function ( EventInterface $event ) {
			return is_a( $event, LootEvent::class );
		} );
	}

	/**
	 * @return Player[]
	 */
	public function getPlayers() {
		return $this->players;
	}

	/**
	 * @return DateTime
	 */
	public function getFirstEventTime() {
		if ( count( $this->events ) ) {
			return $this->events[0]->getEventTime();
		} else {
			return null;
		}
	}

}