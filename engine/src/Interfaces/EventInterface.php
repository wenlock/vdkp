<?php


namespace vDKP\Interfaces;


use DateTime;
use vDKP\Models\Player;

interface EventInterface {
	public function __construct( DateTime $eventTime );

	/**
	 * @return DateTime
	 */
	public function getEventTime();

	public function getEventText();

	/**
	 * Get the players attended the event
	 *
	 * @return Player[]
	 */
	public function getPlayers();

	/**
	 * @return array
	 */
	public function getClassCount();
}