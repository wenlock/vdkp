<?php


namespace vDKP\Interfaces;


use vDKP\Models\Player;
use vDKP\Models\State;

interface AwardInterface {

	/**
	 * AwardInterface constructor.
	 *
	 * @param   float  $modifier
	 * @param   array  $fixedValues
	 */
	public function __construct( float $modifier = 1, array $fixedValues = [] );

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 * @param   Player          $player
	 *
	 * @return float
	 */
	public function __invoke( Player $player, EventInterface $event, State $state );
}