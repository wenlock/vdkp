<?php


namespace vDKP\Interfaces;


use vDKP\Models\State;
use vDKP\Models\Transaction;

interface RuleInterface {

	/**
	 * RuleInterface constructor.
	 *
	 * @param   AwardInterface  $award
	 * @param   array           $config
	 */
	public function __construct( AwardInterface $award, array $config = [] );

	/**
	 * @return string
	 */
	public function getName();

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event );
}