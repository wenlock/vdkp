<?php


namespace vDKP;

use DateTime;

class WowTime {
	const STARTTIME = 1101164400;
	const PRECISION = 120;

	public static function from( $wowTime ) {
		return ( $wowTime * self::PRECISION ) + self::STARTTIME;
	}

	public static function to( DateTime $time = null ) {
		if ( is_null( $time ) ) {
			$time = new DateTime();
		}

		return round( ( $time->getTimestamp() - self::STARTTIME ) / self::PRECISION );
	}

}