<?php

namespace vDKP;

use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use League\Csv\Reader;
use League\Csv\Writer;
use vDKP\Events\WeekEndEvent;
use vDKP\Events\WeekStartEvent;
use vDKP\Import\DKPStringImport;
use vDKP\Interfaces\EventInterface;
use vDKP\Models\RaidBreak;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class Utils {

    static function getTimeDiffInMinutes(EventInterface $startEvent, EventInterface $endEvent) {
        $to_time = $startEvent->getEventTime()->getTimestamp();
        $from_time = $endEvent->getEventTime()->getTimestamp();

        return round(abs($to_time - $from_time) / 60, 2);
    }

    /**
     * @param $timestamp
     *
     * @return DateTime|false
     * @throws Exception
     */
    static function fromTimestamp($timestamp) {
        return date_timestamp_set(new DateTime(), (int)$timestamp);
    }

    static function roundUp($value, $places = 0) {
        if ($places < 0) {
            $places = 0;
        }
        $mult = pow(10, $places);

        return ceil($value * $mult) / $mult;
    }

    /**
     * @param EventInterface[]
     */
    static function eventSort(&$events) {
        usort($events, function (EventInterface $a, EventInterface $b) {
            return $a->getEventTime() <=> $b->getEventTime();
        });
    }

    /**
     * @param Transaction[]
     */
    static function transactionSort(&$transaction) {
        usort($transaction, function (Transaction $a, Transaction $b) {
            return $a->getTransactionTime() <=> $b->getTransactionTime();
        });
    }

    static function path() {
        $parts = func_get_args();
        array_unshift($parts, getcwd());

        return implode(DIRECTORY_SEPARATOR, $parts);
    }

    static function fileToArray($path) {
        $content = file_get_contents($path);
        $content = trim($content);
        $content = explode(PHP_EOL, $content);

        return array_map('trim', $content);
    }

    static function toCsv(array $data) {
        $csv = Writer::createFromString('');
        $headers = array_map('ucfirst', array_keys($data[0]));
        $csv->insertOne($headers);
        foreach ($data as $datum) {
            $csv->insertOne(array_values($datum));
        }

        return $csv->getContent();
    }

    static function toJson(array $data) {
        return json_encode($data);
    }

    /**
     * @param DateTime $begin
     * @param DateTime|null $end
     *
     * @return DateTime[]
     * @throws Exception
     */
    static function getWeekDates(DateTime $begin, DateTime $end = null) {
        if (is_null($end)) {
            $end = new DateTime();
        }
        $end = $end->modify('+1 day');
        $interval = new DateInterval('P7D');
        $daterange = new DatePeriod($begin, $interval, $end);
        $dates = [];
        foreach ($daterange as $date) {
            $dates[] = $date;
        }

        return $dates;
    }

    /**
     * @param RuleEngine $engine
     * @param               $eventFiles
     *
     * @return State
     * @throws Exception
     */
    public static function loadDkpFiles(RuleEngine $engine, array $eventFiles) {
        $dkpStrings = [];
        foreach ($eventFiles as $path) {
            $dkpStrings[] = file_get_contents($path);
        }

        return self::loadDkpStrings($engine, $dkpStrings);
    }

    /**
     * @param RuleEngine $engine
     * @param array $dkpStrings
     *
     * @return State
     * @throws Exception
     */
    public static function loadDkpStrings(RuleEngine $engine, array $dkpStrings) {
        $eventGroups = [];
        foreach ($dkpStrings as $dkpString) {
            $dkp = new DKPStringImport($dkpString);
            $firstEventTime = $dkp->getFirstEventTime();
            if ($firstEventTime) {
                $time = $dkp->getFirstEventTime()->getTimestamp();
                $eventGroups[$time] = $dkp->getAllEvents();
            }
        }
        $firstTime = min(array_keys($eventGroups));
        $firstDateTime = date_timestamp_set(date_create(), $firstTime);
        $firstDateTime->modify('next wednesday');
        foreach (Utils::getWeekDates($firstDateTime) as $weekStart) {
            if ($weekStart < new \DateTime()) {
                $weekEnd = clone $weekStart;
                $weekEnd->modify("-1 second");
                $eventGroups[$weekEnd->getTimestamp()] = [new WeekEndEvent($weekEnd)];
                $eventGroups[$weekStart->getTimestamp()] = [new WeekStartEvent($weekStart)];
            }
        }
        ksort($eventGroups);

        /* @var RuleEngine $engine */
        $state = new State();
        foreach ($eventGroups as $eventGroup) {
            $state = $engine->calculate($eventGroup, $state);
        }

        return $state;
    }

    static function csvToAssocArray($filename) {
        $reader = Reader::createFromPath($filename, 'r');
        $records = $reader->getRecords();
        $output = [];
        foreach ($records as $offset => $record) {
            list($name, $value) = $record;
            $output[$name] = $value;
        }

        return $output;
    }

}