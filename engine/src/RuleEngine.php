<?php

namespace vDKP;

use vDKP\Awards\AbstractAward;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;
use vDKP\Rules\Adjustment;
use vDKP\Rules\Freeze;
use vDKP\Rules\Transfer;

class RuleEngine {

	public function __construct() {
		// Static rules to solve adjustments
		$this->addRule( new Adjustment() );
		$this->addRule( new Transfer() );
		$this->addRule( new Freeze() );
	}

	/**
	 * @var RuleInterface[]
	 */
	private $rules = [];

	/**
	 * @param   RuleInterface  $rule
	 */
	public function addRule( RuleInterface $rule ) {
		$this->rules[ $rule->getName() ] = $rule;
	}

	public function calculate( array $events, State $state = null ) {
		if ( is_null( $state ) ) {
			$state = new State();
		}
		Utils::eventSort($events);
		foreach ( $events as $event ) {
			$transactions = [];
			foreach ( $this->getRules() as $ruleIndex => $rule ) {
				$transactions = array_merge( $transactions, self::applyRule( $rule, $state, $event ) );
			}
			$state->mutate( $event, $transactions );
		}

		return $state;
	}

	/**
	 * @return RuleInterface[]
	 */
	public function getRules() {
		return $this->rules;
	}

	/**
	 * @param   RuleInterface   $rule
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public static function applyRule(
		RuleInterface $rule,
		State $state,
		EventInterface $event
	) {
		return call_user_func_array( $rule, [ $state, $event ] );
	}
}