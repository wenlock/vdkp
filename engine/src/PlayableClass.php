<?php


namespace vDKP;


class PlayableClass {
	const WARRIOR = "WARRIOR";
	const ROGUE = "ROGUE";
	const MAGE = "MAGE";
	const PRIEST = "PRIEST";
	const WARLOCK = "WARLOCK";
	const HUNTER = "HUNTER";
	const DRUID = "DRUID";
	const PALADIN = "PALADIN";
}