<?php

namespace vDKP\Abstracts;

use ReflectionClass;
use vDKP\Interfaces\AwardInterface;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\Player;
use vDKP\Models\State;
use vDKP\Models\Transaction;

abstract class AbstractRule implements RuleInterface {

    const CONF_NAME = "name";
    /**
     * @var AwardInterface
     */
    var $award;
    /**
     * @var array
     */
    var $config;

    var $name;

    public function __construct(AwardInterface $award = null, array $config = []) {
        $this->award = $award;
        $this->config = $config;
    }

    public function getConfig($key, $default = null) {
        if (array_key_exists($key, $this->config)) {
            return $this->config[$key];
        } else {
            return $default;
        }
    }

    public function getName() {
        if (!is_null($this->getConfig(self::CONF_NAME))) {
            return $this->getConfig(self::CONF_NAME);
        } else {
            return (new ReflectionClass(get_called_class()))->getShortName();
        }
    }

    public function getAward(Player $player, EventInterface $event, State $state) {
        return call_user_func_array($this->award, [$player, $event, $state]);
    }

    public function createTransaction(Player $player, EventInterface $event, State $state) {
        $transaction = new Transaction($player, $event, $this);
        $transaction->setAmount($this->getAward($player, $event, $state));

        return $transaction;
    }
}