<?php


namespace vDKP\Abstracts;


use DateTime;
use vDKP\Models\Player;

abstract class AbstractEvent {


	/**
	 * @var DateTime
	 */
	var $eventTime;

	/**
	 * @var Player[]
	 */
	var $players = [];

	/**
	 * AbstractEvent constructor.
	 *
	 * @param   DateTime  $eventTime
	 */
	public function __construct( DateTime $eventTime ) {
		$this->eventTime = $eventTime;
	}

	/**
	 * @return DateTime
	 */
	public function getEventTime() {
		return $this->eventTime;
	}

	public function addPlayer( Player $player ) {
		$this->players[ $player->name ] = $player;
	}

	/**
	 * @return Player[]
	 */
	public function getPlayers() {
		return array_values( $this->players );
	}

	/**
	 * @param   int  $index
	 *
	 * @return Player
	 */
	public function getPlayer($index=0){
		return $this->getPlayers()[$index];
	}
	public function getClassCount() {
		$classCount = [];
		foreach ( $this->getPlayers() as $player ) {
			if ( ! array_key_exists( $player->class, $classCount ) ) {
				$classCount[ $player->class ] = 0;
			}
			$classCount[ $player->class ] ++;
		}

		return $classCount;
	}
}