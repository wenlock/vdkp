<?php


namespace vDKP\Abstracts;


abstract class AbstractAward {

	var $modifier;
	var $fixedValues;

	public function __construct( float $modifier = 1, array $fixedValues = [] ) {
		$this->modifier    = $modifier;
		$this->fixedValues = $fixedValues;
	}

}