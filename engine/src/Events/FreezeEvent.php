<?php


namespace vDKP\Events;


use vDKP\Abstracts\AbstractEvent;
use vDKP\Interfaces\EventInterface;

class FreezeEvent extends AbstractEvent implements EventInterface {
	var $value = 0;
	var $text = "";

	/**
	 * @return int
	 */
	public function getValue(): int {
		return $this->value;
	}

	/**
	 * @param   int  $value
	 */
	public function setValue( int $value ): void {
		$this->value = $value;
	}

	public function getEventText(): string {
		return "characters points was frozen to " . $this->getValue() . "p";
	}
}