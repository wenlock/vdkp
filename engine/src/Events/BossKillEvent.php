<?php

namespace vDKP\Events;


use vDKP\Abstracts\AbstractEvent;
use vDKP\Interfaces\EventInterface;

class BossKillEvent extends AbstractEvent implements EventInterface {

	var $bossName;
	var $instanceName;
	var $loot = [];

	public function getEventText() {
		return $this->bossName . " was killed";
	}
}