<?php


namespace vDKP\Events;


use DateTime;
use vDKP\Abstracts\AbstractEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Models\Player;

class TransferEvent extends AbstractEvent implements EventInterface {
	var $fromPlayer;
	var $toPlayer;

    /**
     * @var DateTime
     */
    var $startTime;

    /**
     * @return DateTime | null
     */
    public function getStartTime() {
        return $this->startTime;
    }

    /**
     * @param DateTime $startTime
     */
    public function setStartTime(DateTime $startTime): void {
        $this->startTime = $startTime;
    }

	/**
	 * @return Player
	 */
	public function getFromPlayer(): Player {
		return $this->fromPlayer;
	}

	/**
	 * @param   Player  $fromPlayer
	 */
	public function setFromPlayer( Player $fromPlayer ): void {
		$this->fromPlayer = $fromPlayer;
	}

	/**
	 * @return Player
	 */
	public function getToPlayer(): Player {
		return $this->toPlayer;
	}

	/**
	 * @param   Player  $toPlayer
	 */
	public function setToPlayer( Player $toPlayer ): void {
		$this->toPlayer = $toPlayer;
	}


	public function getEventText() {
		return "points was transfered from " . $this->fromPlayer->name . " to " . $this->toPlayer->name;
	}
}