<?php

namespace vDKP\Events;

use vDKP\Abstracts\AbstractEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Models\Player;

class LootEvent extends AbstractEvent implements EventInterface {

	var $itemName;
	var $buyer;


	var $bossName;
	var $value = 0;
	var $isMinimumDkp = false;
	var $isGuildBank = true;

	public function getEventText() {
		if($this->getBuyer()){
			return $this->getBuyer()->name . " looted " . $this->itemName . " from " . $this->bossName;
		} elseif($this->isGuildBank){
			return $this->itemName . " was put in the guildbank";
		}

	}

	public function isMinimumDkp( bool $bool = true ) {
		$this->isMinimumDkp = $bool;
	}

	/**
	 * @return Player
	 */
	public function getBuyer() {
		return $this->buyer;
	}

	/**
	 * @param   Player  $buyer
	 */
	public function setBuyer( $buyer ): void {
		$this->buyer = $buyer;
	}
}