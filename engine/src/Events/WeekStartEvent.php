<?php

namespace vDKP\Events;

use vDKP\Abstracts\AbstractEvent;
use vDKP\Interfaces\EventInterface;

class WeekStartEvent extends AbstractEvent implements EventInterface {

	public function getEventText() {
		$raidweek = $this->getEventTime()->format("W");
		return "raid week $raidweek started";
	}

}