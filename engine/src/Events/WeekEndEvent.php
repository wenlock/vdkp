<?php

namespace vDKP\Events;

use vDKP\Abstracts\AbstractEvent;
use vDKP\Interfaces\EventInterface;

class WeekEndEvent extends AbstractEvent implements EventInterface {

	public function getEventText() {
        $last = clone $this->getEventTime();
        $last->sub(new \DateInterval('P7D'));
        $raidweekended = $last->format("W");
		return "raid week $raidweekended ended";
	}

}
