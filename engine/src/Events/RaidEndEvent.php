<?php


namespace vDKP\Events;

use vDKP\Abstracts\AbstractEvent;
use vDKP\Interfaces\EventInterface;

/**
 * Class RaidEndEvent
 * Used to calculate minute DKP for progress-raids.
 *
 * @package vDKP\Models
 */
class RaidEndEvent extends AbstractEvent implements EventInterface {

	public function getEventText() {
		return "";
	}


}