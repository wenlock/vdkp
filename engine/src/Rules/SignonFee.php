<?php


namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class SignonFee extends AbstractRule implements RuleInterface {

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		$allPlayersWithDkp = array_keys( $state->getAllPlayers() );
		foreach ( $event->getPlayers() as $player ) {
			if ( ! in_array( $player->name, $allPlayersWithDkp ) ) {
				$transaction = new Transaction( $player, $event, $this );
				$transaction->setAmount( $this->getAward( $player, $event, $state ) );
				$transactions[] = $transaction;
			}
		}
		return $transactions;
	}
}