<?php

namespace vDKP\Rules;

use vDKP\Abstracts\AbstractRule;
use vDKP\Events\RaidEndEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class Wipefest extends AbstractRule implements RuleInterface {
	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, RaidEndEvent::class ) ) {
			return $transactions;
		}

		return $transactions;
	}
}