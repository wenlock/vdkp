<?php


namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\BossKillEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleConfigInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class NthKillBonus extends AbstractRule implements RuleInterface {

	const CONF_BOSS = "boss";
	const CONF_NUMBER_OF_KILLS = "number-of-kills";

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions  = [];
		$boss          = $this->getConfig( self::CONF_BOSS );
		$numberOfKills = $this->getConfig( self::CONF_NUMBER_OF_KILLS );
		if ( ! is_a( $event, BossKillEvent::class ) ) {
			return $transactions;
		}
		/* @var $event BossKillEvent */
		if ( $event->bossName === $boss ) {
			$bossKillCountPrPlayer = [];
			foreach ( $event->getPlayers() as $player ) {
				$bossKillCountPrPlayer[ $player->name ] = 1;
			}
			foreach ( $state->getBossKillsByName( $boss ) as $bossKillEvent ) {
				foreach ( $bossKillEvent->getPlayers() as $player ) {
					if ( ! array_key_exists( $player->name, $bossKillCountPrPlayer ) ) {
						$bossKillCountPrPlayer[ $player->name ] = 0;
					}
					$bossKillCountPrPlayer[ $player->name ] ++;
				}
			}
			foreach ( $event->getPlayers() as $player ) {
				if ( $bossKillCountPrPlayer[ $player->name ] % $numberOfKills === 0 ) {
					$transaction         = new Transaction( $player, $event, $this );
					$transaction->setAmount( $this->getAward( $player, $event, $state ) );
					$transactions[]      = $transaction;
				}
			}
		}

		return $transactions;
	}
}