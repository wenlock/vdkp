<?php

namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\LootEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class DeductLootMinimumBid extends AbstractRule implements RuleInterface {

	var $legendaries = [ "Bindings of the Windseeker" ];

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, LootEvent::class ) || is_null( $event->getBuyer() ) ) {
			return $transactions;
		}
		/* @var $event LootEvent */
		$orginalValue = (int) $event->value;
		$lootValues   = [
			$state->getAvgLootValueByItem( $event->itemName ) * 0.5,
			$orginalValue,
			25
		];
		if ( in_array( $event->itemName, $this->legendaries ) ) {
			$lootValues[] = $state->getPlayerDKP( $event->getBuyer() );
			$lootValues[] = $state->getHigestLootValue();
			$event->value = 0;
		} else {
			$event->isMinimumDkp( $orginalValue != max( $lootValues ) );
			$event->value = max( $lootValues );
		}


		$transaction = new Transaction( $event->getBuyer(), $event, $this );

		$transaction->setAmount( - $event->value );
		$transactions[] = $transaction;


		return $transactions;
	}
}