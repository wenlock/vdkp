<?php

namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\LootEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class TheArnold extends AbstractRule implements RuleInterface {

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, LootEvent::class ) ) {
			return $transactions;
		}
		/** @var $event LootEvent */
		if ( $event->value > $state->getMaxItemPrice( $event->itemName ) ) {
			$player       = $event->getBuyer();
			$arnoldStrike = 1;
			// Then check history...
			foreach ( $state->getLootEventsByPlayer( $player ) as $lootEvent ) {
				if ( $state->getMaxItemPrice( $lootEvent->itemName ) === $lootEvent->value
				     && ! $lootEvent->isMinimumDkp ) {
					$arnoldStrike ++;
				} else {
					break;
				}
			}
			if ( $arnoldStrike % 3 == 0 ) {
				$transactions[] = $this->createTransaction( $player, $event, $this );
			}
		}

		return $transactions;
	}
}