<?php


namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\TransferEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class Transfer extends AbstractRule implements RuleInterface {
	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, TransferEvent::class ) ) {
			return $transactions;
		}
		/* @var $event TransferEvent */
        $points=0;
        if(is_null($event->getStartTime())){
            $points = $state->getPlayerDKP( $event->getFromPlayer() ) - 100;
        } else {
            $fromPlayerTransactions = $state->getTransactionsByPlayer($event->getFromPlayer(), $event->getStartTime(), $event->getEventTime());
            foreach ($fromPlayerTransactions as $transaction){
                $transaction->getRule();
            }
            $toPlayerTransactions = $state->getTransactionsByPlayer($event->getToPlayer(), $event->getStartTime(), $event->getEventTime());

        }
		if ( $points > 0 ) {
			$transaction = new Transaction( $event->getFromPlayer(), $event, $this );
			$transaction->setAmount( - $points );
			$transactions[] = $transaction;
			$transaction    = new Transaction( $event->getToPlayer(), $event, $this );
			$transaction->setAmount( $points );
			$transactions[] = $transaction;
		}
		return $transactions;
	}
}