<?php


namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\BossKillEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\SpeedBonusConfig;
use vDKP\Models\State;
use vDKP\Models\Transaction;
use vDKP\Utils;

class SpeedBonus extends AbstractRule implements RuleInterface {

	const CONF_BOSSES = "bosses";
	const CONF_TIMEFRAME = "timelimit";
	const CONF_AWARD_ALL = "award-all";

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, BossKillEvent::class ) ) {
			return $transactions;
		}
		$targetedBosses = $this->getConfig( self::CONF_BOSSES );
		$timeframe      = $this->getConfig( self::CONF_TIMEFRAME );
		$awardAll       = $this->getConfig( self::CONF_AWARD_ALL );
		/* @var $event BossKillEvent */
		if ( in_array( $event->bossName, $targetedBosses ) ) {
			$success = true;
			foreach ( $targetedBosses as $targetedBossName ) {
				if ( $targetedBossName != $event->bossName ) {
					$lastTargetedBossKill = $state->getLastBossKillByName( $targetedBossName );
					if ( $lastTargetedBossKill ) {
						$timeDiff = Utils::getTimeDiffInMinutes( $lastTargetedBossKill, $event );
						if ( $timeDiff > $timeframe ) {
							$success = false;
						}
					} else {
						$success = false;
					}
				}
			}
			if ( $success ) {
				echo( "Weeeeh! " . PHP_EOL );
				foreach ( $event->getPlayers() as $player ) {
					$transaction = new Transaction( $player, $event, $this );
					$transaction->setAmount( $this->getAward( $player, $event, $state ) );
					$transactions[] = $transaction;
				}
			}
			if ( $success && $awardAll ) {
				foreach ( $targetedBosses as $earlierBossname ) {
					$earlierBossKillEvent = $state->getLastBossKillByName( $earlierBossname );
					if ( $earlierBossname != $event->bossName && $earlierBossKillEvent ) {
						foreach ( $earlierBossKillEvent->getPlayers() as $player ) {
							// echo($earlierBossKillEvent->getEventText()." => ".$this->award->modifier.PHP_EOL);
							$transaction = new Transaction( $player, $earlierBossKillEvent, $this );
							$transaction->setAmount( $this->getAward( $player, $earlierBossKillEvent, $state ) );
							$transactions[] = $transaction;
						}
					}
				}
			}
		}

		return $transactions;
	}

}