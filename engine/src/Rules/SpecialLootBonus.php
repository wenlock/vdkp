<?php


namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\LootEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleConfigInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class SpecialLootBonus extends AbstractRule implements RuleInterface {

	const CONF_ITEMNAMES = "itemnames";

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, LootEvent::class ) ) {
			return $transactions;
		}
		/* @var $event LootEvent */
		$itemNames = $this->getConfig( self::CONF_ITEMNAMES );
		if ( in_array( $event->itemName, $itemNames ) ) {
			foreach ( $event->getPlayers() as $player ) {
				if ( $player->name !== $event->getBuyer()->name ) {
					$transactions[] = $this->createTransaction( $player, $event, $state );
				}
			}
		}

		return $transactions;
	}
}