<?php


namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\BossKillEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class CrowdedBonus extends AbstractRule implements RuleInterface {

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, BossKillEvent::class ) ) {
			return $transactions;
		}
		$classCount = $event->getClassCount();
		foreach ( $event->getPlayers() as $player ) {
			if ( $classCount[ $player->class ] >= 6 ) {
				$transaction         = new Transaction( $player, $event, $this );
				$transaction->setAmount( $this->getAward( $player, $event, $state ) );
				$transactions[]      = $transaction;
			}
		}

		return $transactions;
	}
}