<?php


namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\LootEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class NonProgressKickback extends AbstractRule implements RuleInterface {

	const CONF_ITEMNAMES = "itemnames";

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, LootEvent::class ) || is_null( $event->getBuyer() ) ) {
			return $transactions;
		}
		/* @var $event LootEvent */
		$itemNames = $this->getConfig( self::CONF_ITEMNAMES );
		if ( ! in_array( $event->itemName, $itemNames ) && $event->value <= 50 ) {
			$transactions[] = $this->createTransaction( $event->getBuyer(), $event, $state );
		}

		return $transactions;
	}
}