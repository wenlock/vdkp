<?php


namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\BossKillEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class FirstKillBonus extends AbstractRule implements RuleInterface {

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, BossKillEvent::class ) ) {
			return $transactions;
		}
		/* @var $event BossKillEvent */
		if ( ! array_key_exists( $event->bossName, $state->getLastBossKills() ) ) {
			foreach ( $event->getPlayers() as $player ) {
				$transaction         = new Transaction( $player, $event, $this );
				$transaction->setAmount( $this->getAward( $player, $event, $state ) );

				$transactions[]      = $transaction;
			}
		}

		return $transactions;
	}
}