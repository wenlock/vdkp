<?php

namespace vDKP\Rules;

use Exception;
use vDKP\Abstracts\AbstractRule;
use vDKP\Events\WeekStartEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\RaidBreak;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class WeeklyDecay extends AbstractRule implements RuleInterface {

    const CONF_BREAKS = "breaks";

    /**
     * @return RaidBreak[]
     */
    public function getBreaks() {
        return $this->getConfig(self::CONF_BREAKS, []);
    }

    /**
     * @param State $state
     * @param EventInterface $event
     *
     * @return Transaction[]
     * @throws Exception
     */
    public function __invoke(State $state, EventInterface $event) {
        $transactions = [];
        if (!is_a($event, WeekStartEvent::class)) {
            return $transactions;
        }
        $playersOnBreak = [];
        foreach ($this->getBreaks() as $break) {
            if ($break->start < $event->getEventTime() && $break->end > $event->getEventTime()) {
                if ($break->isForAllPlayers()) {
                    return $transactions;
                } else {
                    $playersOnBreak = array_merge($playersOnBreak, $break->getForWho());
                }

            }
        }
        foreach ($state->getAllPlayers() as $player) {
            $transaction = $this->createTransaction($player, $event, $state);
            $transactions[] = $transaction;
            if (in_array($player->name, $playersOnBreak)) {
                /**
                 * Player break should be implemented in another rule.
                 */
                $breakRefundTransaction = clone $transaction;
                $breakRefundTransaction->setAmount(abs($transaction->getAmount()));
                $breakRefundTransaction->eventText = "player on break WeeklyDecay refunded";
                $transactions[] = $breakRefundTransaction;
                echo $breakRefundTransaction->getTitle().PHP_EOL;
            }
        }
        return $transactions;
    }
}