<?php

namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\LootEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class DeductLoot extends AbstractRule implements RuleInterface {

	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, LootEvent::class ) || is_null( $event->getBuyer() ) ) {
			return $transactions;
		}
		/* @var $event LootEvent */
		$transaction = new Transaction( $event->getBuyer(), $event, $this );
		$transaction->setAmount( - $event->value );
		$transactions[] = $transaction;

		return $transactions;
	}
}