<?php


namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\AdjustmentEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class Adjustment extends AbstractRule implements RuleInterface {
	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, AdjustmentEvent::class ) ) {
			return $transactions;
		}
		/* @var $event AdjustmentEvent */
		foreach ( $event->getPlayers() as $player ) {
			$transaction = new Transaction( $player, $event, $this );
			$transaction->setAmount( $event->getValue() );
			$transactions[] = $transaction;
		}

		return $transactions;
	}
}