<?php

namespace vDKP\Rules;

use vDKP\Abstracts\AbstractRule;
use vDKP\Events\WeekEndEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\Player;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class MultiCharTransfer extends AbstractRule implements RuleInterface {

    const CONF_CHARS = "multi-char-transfers";
    const CONF_ON_RULES = "transfer-on-rules";

    public function getCharConfigs() {
        return $this->getConfig(self::CONF_CHARS, []);
    }

    /**
     * @return array
     */
    public function getRulesForTransfer() {
        return $this->getConfig(self::CONF_ON_RULES, []);
    }

    /**
     * @inheritDoc
     */
    public function __invoke(State $state, EventInterface $event) {
        $transactions = [];
        if (!is_a($event, WeekEndEvent::class)) {
            return $transactions;
        }
        $charConfigs = $this->getCharConfigs();
        $startTime = clone $event->getEventTime();
        $startTime->sub(new \DateInterval("P7D"));
        $transactionsLastWeek = $state->getTransactionsBetween(
            $startTime,
            $event->getEventTime(),
            $this->getRulesForTransfer(),
            );
        if (count($transactionsLastWeek) === 0) {
            return $transactions;
        }
        $playerPointsEarned = [];
        foreach ($transactionsLastWeek as $transaction) {
            $playerName = $transaction->getPlayerName();
            if (!array_key_exists($playerName, $playerPointsEarned)) {
                $playerPointsEarned[$playerName] = [];
            }
            $playerPointsEarned[$playerName][] = $transaction->getAmount();
        }
        foreach ($playerPointsEarned as $player => $sums) {
            $playerPointsEarned[$player] = array_sum($sums);
        }
        $maxPointsPossible = max($playerPointsEarned);
        echo $event->getEventText() . ". last week max points is " . $maxPointsPossible . PHP_EOL;
        // go through all availiable chargroups.
        foreach ($charConfigs as $charConfig) {
            $playerNames = explode(",", $charConfig);
            $pool = [];
            // Prep the player Pool
            foreach ($playerNames as $playerName) {
                $player = new Player($playerName);
                $playerMaxDkp = $state->getPlayerDKP($player);
                if (array_key_exists($playerName, $playerPointsEarned)) {
                    $v = $playerMaxDkp > 0 ? $playerMaxDkp : 0;
                    $o = $playerPointsEarned[$playerName] > 0 ? $playerPointsEarned[$playerName] : 0;
                    $pool[$playerName] = min([$o, $v]);
                } else {
                    $pool[$playerName] = 0;
                }
            }
            $playerTotalPointsLeft = array_sum($pool);
            $control = [];
            foreach ($playerNames as $playerName) {
                $player = new Player($playerName);
                $playerPointsDisponible = $pool[$playerName];
                $willBeTransfered = 0;
                // withdraw point from pool
                $playerTotalPointsLeft = $playerTotalPointsLeft - $playerPointsDisponible;
                $neededToReachMax = $maxPointsPossible - $playerPointsDisponible;

                // player is in plus and character need a transfer
                if ($neededToReachMax > 0 && $playerTotalPointsLeft > 0) {
                    if ($neededToReachMax < $playerTotalPointsLeft) {
                        // enough points in the pool, transdfer what is needed.
                        $willBeTransfered = $neededToReachMax;
                    } else {
                        // what is left will be transfered
                        $willBeTransfered = $playerTotalPointsLeft;
                    }
                    // points are adjustet back into the pool
                    $playerTotalPointsLeft = $playerTotalPointsLeft - $willBeTransfered;
                } else if ($playerTotalPointsLeft <= 0) {
                    // players pool is in minus... need to transfer from char
                    if ($playerPointsDisponible >= abs($playerTotalPointsLeft)) {
                        $willBeTransfered = $playerTotalPointsLeft;
                    } else {
                        $willBeTransfered = -$playerPointsDisponible;
                    }
                }

                if ($willBeTransfered !== 0) {
                    $transaction = new Transaction($player, $event, $this);
                    $transaction->setAmount($willBeTransfered);
                    $transactions[] = $transaction;
                    $control[] = $willBeTransfered;
                    echo "Needed to reach max for $playerName($playerPointsDisponible) is $neededToReachMax pool is $playerTotalPointsLeft";
                    echo " transfered $willBeTransfered points" . PHP_EOL;
                }
            }
            if (array_sum($control) !== 0) {
                var_dump($control);
                exit();
            }
        }
        return $transactions;
    }
}