<?php


namespace vDKP\Rules;


use vDKP\Abstracts\AbstractRule;
use vDKP\Events\FreezeEvent;
use vDKP\Interfaces\EventInterface;
use vDKP\Interfaces\RuleInterface;
use vDKP\Models\State;
use vDKP\Models\Transaction;

class Freeze extends AbstractRule implements RuleInterface {
	/**
	 * @param   State           $state
	 * @param   EventInterface  $event
	 *
	 * @return Transaction[]
	 */
	public function __invoke( State $state, EventInterface $event ) {
		$transactions = [];
		if ( ! is_a( $event, FreezeEvent::class ) ) {
			return $transactions;
		}

		/* @var $event FreezeEvent */
		foreach ( $event->getPlayers() as $player ) {
			$playerDkp   = $state->getPlayerDKP( $player );
			$transaction = new Transaction( $player, $event, $this );
			$transaction->setAmount(   $event->getValue() - $playerDkp);
			$transactions[] = $transaction;
		}

		return $transactions;
	}
}