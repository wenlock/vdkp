<?php


namespace vDKP\Export;

use League\Csv\Writer;

class CsvExport {
	public function __invoke( array $data ) {
		$csv     = Writer::createFromString( '' );
		$headers = array_map( 'ucfirst', array_keys( $data[0] ) );
		$csv->insertOne( $headers );
		foreach ( $data as $datum ) {
			$csv->insertOne( array_values( $datum ) );
		}

		return $csv->getContent();
	}
}