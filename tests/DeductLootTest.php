<?php

use PHPUnit\Framework\TestCase;
use vDKP\Bosses;
use vDKP\Create;
use vDKP\Events\LootEvent;
use vDKP\Models\State;
use vDKP\Models\Transaction;
use vDKP\RuleEngine;
use vDKP\Rules\DeductLoot;
use vDKP\Simulator;

class DeductLootTest extends TestCase {

	public function test__invoke() {
		$state = new State();
		$simulator = new Simulator();
		$lootValue = 100;
		$event = Create::lootEvent(Bosses::LUCIFRON, $lootValue);
		$award = new \vDKP\Awards\BossLootPrice();
		$event->addPlayer($simulator->getRandomPlayer());
		$event->itemName   = "item";
		$transactions      = RuleEngine::applyRule( new DeductLoot( $award ), $state, $event );
		$this->assertIsArray( $transactions );
		$this->assertContainsOnly( Transaction::class, $transactions );
		$this->assertEquals( $event->getPlayers()[0], $transactions[0]->player );
		$this->assertEquals(-$lootValue, $transactions[0]->amount);
	}
}
