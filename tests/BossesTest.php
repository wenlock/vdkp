<?php

use PHPUnit\Framework\TestCase;
use vDKP\Bosses;

class BossesTest extends TestCase {

	public function testWillReturnTrue() {
		$bosses = new Bosses();
		$this->assertTrue( $bosses->willReturnTrue() );
	}
}
