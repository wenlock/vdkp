<?php


use PHPUnit\Framework\TestCase;
use vDKP\Awards\PercentOfPlayerDkp;
use vDKP\Create;
use vDKP\Events\WeekStartEvent;
use vDKP\Models\State;
use vDKP\Models\Transaction;
use vDKP\RuleEngine;
use vDKP\Rules\WeeklyDecay;

class WeeklyDecayTest extends TestCase {

	public function test__invoke() {
		$state          = new State();
		$event          = new WeekStartEvent( new DateTime() );
		$award          = new PercentOfPlayerDkp( - 0.15 );
		$playerName     = "Dummy";
		$transactions   = [];
		$transactions[] = Create::transaction( $playerName, 100 );
		$transactions[] = Create::transaction( $playerName, 100 );
		$transactions[] = Create::transaction( "PlayerX", 125 );
		$state->mutate( $event, $transactions );
		$transactions = RuleEngine::applyRule( new WeeklyDecay($award), $state, $event );
		$this->assertIsArray( $transactions );
		$this->assertContainsOnly( Transaction::class, $transactions );
		$this->assertEquals( - 30, $transactions[0]->amount );
	}
}
