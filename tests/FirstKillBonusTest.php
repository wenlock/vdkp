<?php


use PHPUnit\Framework\TestCase;
use vDKP\Bosses;
use vDKP\Create;
use vDKP\Models\State;
use vDKP\Models\Transaction;
use vDKP\RuleEngine;
use vDKP\Rules\FirstKillBonus;
use vDKP\Simulator;

class FirstKillBonusTest extends TestCase {

	public function test__invoke() {
		$state     = new State();
		$simulator = new Simulator();
		$bossName  = Bosses::LUCIFRON;
		$award     = new vDKP\Awards\PercentOfBossDkp( 0.5 );
		$state->mutate( Create::lootEvent( $bossName, 100 ), [] );
		$rule  = new FirstKillBonus( $award );
		$event = Create::bossKillEvent( $bossName );
		$event->addPlayer( $simulator->getRandomPlayer() );
		$event->addPlayer( $simulator->getRandomPlayer() );
		$event->addPlayer( $simulator->getRandomPlayer() );
		$transactions = RuleEngine::applyRule( $rule, $state, $event );
		$this->assertSameSize( $event->getPlayers(), $transactions );
		$this->assertIsArray( $transactions );
		$this->assertContainsOnly( Transaction::class, $transactions );
		foreach ( $transactions as $transaction ) {
			$this->assertEquals( 100 * 0.5, $transaction->amount );
		}

	}
}
