<?php


use PHPUnit\Framework\TestCase;
use vDKP\Bosses;
use vDKP\Create;
use vDKP\Events\BossKillEvent;
use vDKP\Models\State;
use vDKP\Models\Transaction;
use vDKP\PlayableClass;
use vDKP\RuleEngine;
use vDKP\Rules\CrowdedBonus;
use vDKP\Simulator;

class CrowdedTest extends TestCase {

	public function test__invoke() {
		$state     = new State();
		$event     = new BossKillEvent( new DateTime() );
		$award     = new \vDKP\Awards\BossLootPrice( 0.2 );
		$bossName  = Bosses::LUCIFRON;
		$simulator = new Simulator();

		$state->mutate( Create::lootEvent( $bossName, 100 ), [] );
		$state->mutate( Create::bossKillEvent( $bossName ), [] );
		for ( $i = 1; $i <= 6; $i ++ ) {
			$event->addPlayer( $simulator->getRandomPlayer( PlayableClass::PALADIN ) );
		}
		for ( $i = 1; $i <= 2; $i ++ ) {
			$event->addPlayer( $simulator->getRandomPlayer( PlayableClass::WARLOCK ) );
		}
		$event->bossName = $bossName;
		$transactions    = RuleEngine::applyRule( new CrowdedBonus( $award ), $state, $event );
		$this->assertIsArray( $transactions );
		$this->assertContainsOnly( Transaction::class, $transactions );
		foreach ( $transactions as $transaction ) {
			if ( $transaction->player->class === PlayableClass::PALADIN ) {
				$this->assertEquals( 100 * 0.2, $transaction->amount );
			}
		}
	}
}
