<?php


use PHPUnit\Framework\TestCase;
use vDKP\Bosses;
use vDKP\Create;
use vDKP\RuleEngine;
use vDKP\Rules\NthKillBonus;
use vDKP\Simulator;

class NthKillBonusTest extends TestCase {

	public function test__invoke() {
		$state     = null;
		$simulator = new Simulator();
		$lootValue = 100;
		$engine    = new RuleEngine();
		$bossName  = Bosses::LUCIFRON;
		$event     = Create::lootEvent( $bossName, $lootValue );
		$award     = new \vDKP\Awards\BossLootPrice( 0.5 );
		$rule      = new NthKillBonus( $award, [
			NthKillBonus::CONF_NUMBER_OF_KILLS => 3,
			NthKillBonus::CONF_BOSS            => $bossName,
		] );
		$engine->addRule( $rule );
		$player = $simulator->getRandomPlayer();
		for ( $i = 1; $i <= 10; $i ++ ) {
			$lootEvent     = Create::lootEvent( $bossName, 100 );
			$bossKillEvent = Create::bossKillEvent( $bossName );
			$bossKillEvent->addPlayer( $player );
			$bossKillEvent->addPlayer( $simulator->getRandomPlayer() );
			$state = $engine->calculate( [ $lootEvent, $bossKillEvent ] );


		}
		print_r( $state->transactions );
	}
}
