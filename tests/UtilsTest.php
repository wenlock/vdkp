<?php


use PHPUnit\Framework\TestCase;
use vDKP\Utils;

class UtilsTest extends TestCase {
	public function test_that_we_get_date_periods() {
		$startDate = '2019-10-07';
		$datetimes = Utils::getWeekDates( new DateTime( $startDate ), new DateTime( '2019-10-20' ) );
		$this->assertCount( 2, $datetimes );
		foreach ( $datetimes as $datetime ) {
			$this->assertSame( DateTime::class, get_class( $datetime ) );
		}
		$this->assertEquals( $datetimes[0]->getTimestamp(), strtotime( $startDate ) );
	}
}
