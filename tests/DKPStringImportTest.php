<?php


use PHPUnit\Framework\TestCase;
use vDKP\Import\DKPStringImport;

class DKPStringImportTest extends TestCase {
	function test_that_import_works() {
		$dkpString = file_get_contents( dirname( __FILE__ ) . "/vendetta/raid1.xml" );
		$dkp       = new DKPStringImport( $dkpString );
		$this->assertIsObject( $dkp );
		$this->assertIsArray( $dkp->attendance );
		$this->assertIsArray( $dkp->getOtherEvents() );
		$this->assertIsArray( $dkp->getLootEvents() );
		$this->assertIsArray( $dkp->players );
		$this->assertGreaterThan( 2, count( $dkp->attendance ) );
		$this->assertGreaterThan( 2, count( $dkp->getOtherEvents() ) );
		$this->assertGreaterThan( 2, count( $dkp->getLootEvents() ) );
		$this->assertGreaterThan( 2, count( $dkp->players ) );
	}
}
