<?php

use VkpNinja\Db;
use VkpNinja\Util;

require_once "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

if (Util::isDevelopment()) {
    call_user_func_array(['\GaeFlow\Router', 'loadDevEnv'], []);
}

return [
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'development',
        'development' => [
            'name' => Db::getDbName(),
            'connection' => Db::getPdo()
        ],
    ],
    'version_order' => 'creation'
];