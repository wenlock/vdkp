module.exports = {
  "configureWebpack": {
    "resolve": {
      "alias": {
        "@": "/Users/michael/Code/vendetta/vdkp/frontend/src"
      },
      "extensions": [
        ".js",
        ".vue"
      ]
    }
  },
  "devServer": {
    "proxy": {
      "/api": {
        "target": "http://localhost:2004"
      }
    }
  },
  "transpileDependencies": [
    "vuetify"
  ]
}