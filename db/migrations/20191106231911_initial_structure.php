<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class InitialStructure extends AbstractMigration {

    const VARCHAR = MysqlAdapter::PHINX_TYPE_STRING;

    public function change() {
        $tbl_uploads = $this->table('uploads');
        $tbl_uploads->addColumn('guild_id', 'biginteger')
            ->addColumn('dkpstring', 'text')
            ->addColumn('checksum', self::VARCHAR)
            ->addTimestamps()
            ->create();

        $tbl_transactions = $this->table('transactions');
        $tbl_transactions
            ->addColumn("player", self::VARCHAR)
            ->addColumn("time", "datetime")
            ->addColumn("when", self::VARCHAR)
            ->addColumn("value", "integer")
            ->addColumn("event_id", "integer")
            ->addTimestamps()
            ->create();

    }
}
