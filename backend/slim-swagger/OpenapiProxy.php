<?php

namespace SlimSwagger;

use cebe\openapi\exceptions\TypeErrorException;
use cebe\openapi\spec\OpenApi;
use cebe\openapi\spec\Operation;
use cebe\openapi\spec\PathItem;
use cebe\openapi\spec\Paths;
use cebe\openapi\spec\Server;
use Slim\Psr7\Uri;

class OpenapiProxy {

    /**
     * @var OpenApi
     */
    var $openapi;

    public function __construct() {
        $this->openapi = new OpenApi([
            'openapi' => '3.0.2',
            'info' => [
                'title' => 'Test API',
                'version' => '1.0.0',
            ],
            'paths' => [],
        ]);
    }

    /**
     * @param Uri $uri
     * @return Server
     * @throws TypeErrorException
     */
    public function addServerBasedOnUri(Uri $uri): Server {
        $host = $uri->getHost();
        $port = $uri->getPort();
        if (!in_array($port, [80, 443])) {
            $host = $host . ":" . $port;
        }
        $scheme = $port === 443 ? "https://" : "http://";
        $server = new Server([
            "url" => $scheme . $host,
            "description"=> "The production API server"
        ]);
        $this->openapi->servers = [$server];
        return $server;
    }

    /**
     * @return OpenApi
     */
    public function getOpenapi(): OpenApi {
        return $this->openapi;
    }

    /**
     * @return PathItem[]|Paths
     */
    public function getPaths() {
        return $this->openapi->paths;
    }

    /**
     * @param $path
     * @return PathItem
     */
    public function getPath($path) {
        if (!$this->getPaths()->hasPath($path)) {
            $this->getPaths()->addPath($path, new PathItem([]));
        }
        return $this->getPaths()->getPath($path);
    }

    public function addOperation($path, array $methods, Operation $operation) {
        $pathItem = $this->getPath($path);
        foreach ($methods as $method) {
            $method = strtolower($method);
            $pathItem->$method = $operation;
        }

    }
}