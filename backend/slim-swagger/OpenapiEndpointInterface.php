<?php

namespace SlimSwagger;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

interface OpenapiEndpointInterface {

    public function swagger(OpenapiOperation $operation): OpenapiOperation;

    public function __invoke(Request $request, Response $response, array $args);
}