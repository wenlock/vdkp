<?php

namespace SlimSwagger;

use Exception;
use PSX\Model\OpenAPI\Info;
use PSX\Model\OpenAPI\OpenAPI;
use PSX\Model\OpenAPI\Operation;
use PSX\Model\OpenAPI\PathItem;
use PSX\Model\OpenAPI\Paths;

class OpenApiProxy {

    var $openapi;

    public function __construct() {
        $this->openapi = new OpenAPI();
        $this->openapi->setInfo(new Info());
        $this->openapi->setPaths(new Paths());
    }

    /**
     * @return Paths
     */
    public function getPaths() {
        return $this->openapi->getPaths();
    }

    /**
     * @param $path
     * @return PathItem
     */
    public function getPath($path) {
        if (!$this->getPaths()->offsetExists($path)) {
            $this->getPaths()->set($path, new PathItem());
        }
        return $this->getPaths()->offsetGet($path);
    }

    public function addOperation($path, array $methods, Operation $operation) {
        $pathItem = $this->getPath($path);
        foreach ($methods as $method){
            switch ($method) {
                case "GET":
                    $pathItem->setGet($operation);
                    break;
                case "POST":
                    $pathItem->setPost($operation);
                    break;
                case "PUT":
                    $pathItem->setPut($operation);
                    break;
                case "PATCH":
                    $pathItem->setPatch($operation);
                    break;
                case "DELETE":
                    $pathItem->setDelete($operation);
                    break;
                case "OPTIONS":
                    $pathItem->setOptions($operation);
                    break;
                default:
                    throw new Exception("Invalid add operation");
            }
        }

    }
}