<?php

namespace SlimSwagger;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;

class ListRoutes {
    protected $app;

    public function __construct(App $app) {
        $this->app = $app;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception
     */
    public function __invoke(Request $request, Response $response) {
        $routes = $this->app->getRouteCollector()->getRoutes();
        $data = [];
        foreach ($routes as $route) {
            $data[] = [
                "name" => $route->getName(),
                "id" => $route->getIdentifier(),
                "methods" => $route->getMethods(),
                "pattern" => $route->getPattern(),
                "arguments" => $route->getArguments(),
                "invocationStrategy" => $route->getInvocationStrategy(),
                "callable" => get_class($route->getCallable()),
            ];

        }
        $response = $response->withHeader("Content-Type", "application/json");
        $response->getBody()->write(json_encode($data));

        return $response;
    }
}