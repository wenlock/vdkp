<?php

namespace SlimSwagger;

use cebe\openapi\spec\Operation;
use cebe\openapi\Writer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;

class OpenapiAction {

    protected $app;

    public function __construct(App $app) {
        $this->app = $app;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \Exception
     */
    public function __invoke(Request $request, Response $response) {
        $routes = $this->app->getRouteCollector()->getRoutes();
        $openApi = new OpenapiProxy();
        $openApi->addServerBasedOnUri($request->getUri());
        foreach ($routes as $route) {
            $callable = $route->getCallable();
            if (is_subclass_of($callable, OpenapiEndpointInterface::class)) {
                /* @var $callable OpenapiEndpointInterface */
                $operation = new Operation([]);
                $operation->operationId = $route->getIdentifier();
                $operation->tags = [get_class($callable)];
                $operation = $callable->swagger($operation);
                $openApi->addOperation($route->getPattern(), $route->getMethods(), $operation);
            }
        }
        $data  = Writer::writeToJson($openApi->getOpenapi());
        $response = $response->withHeader("Content-Type", "application/json");
        $response->getBody()->write($data);

        return $response;
    }
}