<?php

namespace VkpNinja;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ServeJson {

    private $contentRef;

    public function __construct($jsonSerializebal) {
        $this->contentRef = $jsonSerializebal;
    }

    public function __invoke(Request $request, Response $response) {
        $response = $response->withHeader("Content-Type", "application/json");
        if(is_callable($this->contentRef)){
            $content = call_user_func($this->contentRef);
        } else {
            $content = $this->contentRef;
        }
        $response->getBody()->write(json_encode($content));
        return $response;
    }
}