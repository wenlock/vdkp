<?php

namespace VkpNinja;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response;

class DiscordAuthMiddleware implements MiddlewareInterface {

    var $discord;
    var $unprotectedPaths = [];

    public function __construct(DiscordApi $discord, $unprotectedPaths = []) {
        $this->discord = $discord;
        $this->unprotectedPaths = $unprotectedPaths;
    }

    public function __invoke(Request $request, RequestHandler $handler): ResponseInterface {
        return $this->process($request, $handler);
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param Request $request
     * @param RequestHandler $handler
     * @return ResponseInterface
     */
    public function process(Request $request, RequestHandler $handler): ResponseInterface {
        session_start();
        $response = new Response();
        if ($request->getUri()->getPath() === $this->discord->getRedirectPath()) {
            $this->discord->exchange();
            return Respond::redirect($response, $_SESSION["REDIRECT_AFTER_LOGIN"]);
        } elseif (in_array($request->getUri()->getPath(), $this->unprotectedPaths)) {
            return $handler->handle($request);
        } elseif ($this->discord->getUser()) {
            $existingToken = $this->discord->getToken();
            if ($existingToken && $existingToken->hasExpired()) {
                $this->discord->refreshToken();
            }

            return $handler->handle($request);
        } elseif (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] === "axios") {
            return Respond::error($response, 401);
        } else {
            $uri = $request->getUri();
            if ($uri->getHost() !== "localhost") {
                $uri = $uri->withPort(null);
            }
            $_SESSION["REDIRECT_AFTER_LOGIN"] = (string)$uri;
            $authUri = $this->discord->getAuthorizationUrl();
            return Respond::redirect($response, $authUri);
        }
    }
}