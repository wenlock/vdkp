<?php

namespace VkpNinja;

use League\OAuth2\Client\Token\AccessToken;
use Wohali\OAuth2\Client\Provider\Discord;

class DiscordApi {

    var $options = [];
    var $scope = [];

    /**
     * @var Discord
     */
    var $oAuth2Provider;

    public function __construct($options, $scope = []) {
        $this->options = $options;
        $this->oAuth2Provider = new Discord($options);
        $this->scope = $scope;
    }

    public function getRedirectPath() {
        return parse_url($this->options['redirectUri'], PHP_URL_PATH);
    }

    public function getAuthorizationUrl() {
        return $this->oAuth2Provider->getAuthorizationUrl([
            "scope" => $this->scope
        ]);
    }

    public function exchange() {
        $token = $this->oAuth2Provider->getAccessToken('authorization_code', [
            'code' => $_GET['code']
        ]);
        if ($token) {
            $this->setToken($token);
            $this->setUser($this->oAuth2Provider->getResourceOwner($token)->toArray());
            $this->setGuilds($this->getCurrentUserGuildsFromApi());
        }
    }

    public function refreshToken() {
        syslog(LOG_INFO, "Refreshing access token");
        $token = $this->oAuth2Provider->getAccessToken('refresh_token', [
            'refresh_token' => $this->getToken()->getRefreshToken()
        ]);
        $this->setToken($token);
    }

    public function setToken($token) {
        $_SESSION["token"] = $token;
    }

    /**
     * @return AccessToken || null
     */
    public function getToken() {
        if (isset($_SESSION["token"])) {
            return $_SESSION["token"];
        } else {
            return null;
        }
    }

    public function setUser($user) {
        $_SESSION["user"] = $user;
    }

    public function getUser() {
        if (isset($_SESSION["user"])) {
            $user = $_SESSION["user"];
            $user["avatar_url"] = self::avatarUrl($user["id"], $user["avatar"]);
            return $user;
        } else {
            return null;
        }
    }

    public function setGuilds($guilds) {
        $_SESSION["guilds"] = $guilds;
    }

    public function getGuilds($checkPermissions = []) {
        if (isset($_SESSION["guilds"])) {
            $guilds = [];
            foreach ($_SESSION["guilds"] as $guild) {
                foreach ($checkPermissions as $targetKey => $check) {
                    $guild[$targetKey] = ($guild["permissions"] & $check) ? true : false;
                }
                $guild["icon_url"] = self::iconUrl($guild["id"], $guild["icon"]);
                $guilds[] = $guild;
            }
            return $guilds;
        } else {
            return null;
        }
    }

    public function getFromApi($method, $path) {
        $token = $this->getToken();
        if ($token) {
            $url = $this->oAuth2Provider->apiDomain . $path;
            $request = $this->oAuth2Provider->getAuthenticatedRequest($method, $url, $token);
            return $this->oAuth2Provider->getParsedResponse($request);
        }

        return null;
    }

    public function getCurrentUserGuildsFromApi() {
        return $this->getFromApi(Discord::METHOD_GET, "/users/@me/guilds");
    }

    static function iconUrl($guildId, $iconId) {
        return "https://cdn.discordapp.com/icons/$guildId/$iconId.png";
    }

    static function avatarUrl($user_id, $user_avatar) {
        return "https://cdn.discordapp.com/avatars/$user_id/$user_avatar.png ";

    }
}