<?php

namespace VkpNinja;

class StorageStructure {

    public static function raidlogs($guildId, $filename = null) {
        $parts = [
            "guild",
            $guildId,
            "logs",
        ];
        if (!is_null($filename)) {
            $parts[] = $filename;
        }
        return join("/", $parts);
    }

    public static function results($guildId, $filename = null) {
        $parts = [
            "guild",
            $guildId,
            "results",
        ];
        if (!is_null($filename)) {
            $parts[] = $filename;
        }
        return join("/", $parts);
    }
}