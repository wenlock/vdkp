<?php

namespace VkpNinja;

use Google\Cloud\Storage\StorageClient;
use Slim\App;
use Slim\Factory\AppFactory;
use SlimSwagger\ListRoutes;
use SlimSwagger\OpenapiAction;
use VkpNinja\Endpoint\CurrentUser;
use VkpNinja\Endpoint\CurrentUserGuilds;
use VkpNinja\Endpoint\CurrentUserToken;
use VkpNinja\Endpoint\ListRaidLogs;
use VkpNinja\Endpoint\ReCalculate;
use VkpNinja\Endpoint\SaveRaidLog;
use VkpNinja\Endpoint\TestRouteWithArgs;

class Application {

    /**
     * @param bool $isDev
     * @return App
     */
    public static function bootstrap($isDev = false) {
        $app = AppFactory::create();
        $app->addRoutingMiddleware();
        $scope = [
            'identify',
            'guilds'
        ];
        $discordApi = new DiscordApi([
            'clientId' => getenv("DISCORD_CLIENT_ID"),
            'clientSecret' => getenv("DISCORD_CLIENT_SECRET"),
            'redirectUri' => (string)Util::getFullUri("/__callback"),
        ], $scope);
        $storage = new StorageClient(["suppressKeyFileNotice" => true]);
        $bucket = $storage->bucket(getenv("BUCKET"));
        $state = new ApplicationState($discordApi, $bucket);
        $app->add(new DiscordAuthMiddleware($discordApi, ["/api/env", "/swagger"]));
        $app->get('/api/user', new CurrentUser($state));
        $app->get('/api/token', new CurrentUserToken($state));
        $app->get('/api/guilds', new CurrentUserGuilds($state));
        $app->post(SaveRaidLog::PATTERN, new SaveRaidLog($state));
        $app->get(ListRaidLogs::PATTERN, new ListRaidLogs($state));
        $app->get(ReCalculate::PATTERN, new ReCalculate($state));
        $app->get('/session', new ServeJson(function(){return $_SESSION;}));
        $app->get("/swagger", new OpenapiAction($app));
        $app->get("/_routes", new ListRoutes($app));
        $app->get("/test-with-args", new TestRouteWithArgs($state));
        $app->get('/[{path:.*}]', new ServeStatic(ROOT_DIR . DS . "dist" . DS . "index.html"));
        return $app;
    }
}