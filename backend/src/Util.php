<?php

namespace VkpNinja;

use Cocur\Slugify\Slugify;
use Psr\Http\Message\UriInterface;
use Slim\Psr7\Factory\UriFactory;
use Slim\Psr7\Uri;

class Util {

    /**
     * @param $path
     *
     * @return UriInterface|Uri
     */
    static function getFullUri($path) {
        $uri = (new UriFactory())->createFromGlobals($_SERVER);
        $uri = $uri->withPath($path);
        $uri = $uri->withQuery("");
        if ($uri->getHost() !== "localhost") {
            $uri = $uri->withPort(null);
        }

        return $uri;
    }

    static function compactTime(\DateTime $dateTime) {
        $date = $dateTime->format("c");
        $date = substr($date, 0, -9);
        $date = preg_replace("/[^0-9]/", "", $date);
        return $date;
    }

    static function slugify($string) {
        $slugify = new Slugify();
        return $slugify->slugify($string);
    }

    static function storageUrl($bucket, $name) {
        return join("/", [
            "https://storage.googleapis.com",
            $bucket,
            $name
        ]);
    }

    static function isDevelopment() {
        return getenv("GAE_ENV") === false;
    }

    static function dsnToObj($dsn) {
        $var = str_replace(";", PHP_EOL, $dsn);
        return parse_ini_string($var);
    }
}