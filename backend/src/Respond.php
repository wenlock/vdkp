<?php


namespace VkpNinja;

use Psr\Http\Message\ResponseInterface as Response;

class Respond {

	static public function json( Response $response, $content ) {
		$response = $response->withHeader( "Content-Type", "application/json" );
		$response->getBody()->write( json_encode( $content ) );

		return $response;
	}
    static public function text( Response $response, $content ) {
        $response = $response->withHeader( "Content-Type", "text/plain" );
        $response->getBody()->write( $content );

        return $response;
    }
	static public function error( Response $response, $code, $message = null ) {
		if ( is_null( $message ) ) {
			$dummy   = new \GuzzleHttp\Psr7\Response();
			$dummy   = $dummy->withStatus( $code );
			$message = $dummy->getReasonPhrase();
		}
		$errors   = [
			[
				"code"    => $code,
				"message" => $message,
			]
		];
		$response = $response->withStatus( $code );

		return self::json( $response, $errors );
	}

	static public function redirect( Response $response, $location, $code = 302 ) {
		return $response
			->withHeader( 'Location', $location )
			->withStatus( $code );
	}

}