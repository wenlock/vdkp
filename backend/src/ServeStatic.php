<?php

namespace VkpNinja;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ServeStatic {

    var $filepath;

    public function __construct($filepath) {
        $this->filepath = $filepath;
    }

    public function __invoke(Request $request, Response $response) {
        $content = file_get_contents($this->filepath);
        $contentType = mime_content_type($this->filepath);
        $response = $response->withHeader("content-type", "$contentType; charset=utf-8");
        $response->getBody()->write($content);
        return $response;
    }
}