<?php


namespace VkpNinja;


class AbstractEndpoint {

	/**
	 * @var ApplicationState
	 */
	var $state;

	public function __construct( ApplicationState $state ) {
		$this->state = $state;
	}
}