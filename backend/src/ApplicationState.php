<?php


namespace VkpNinja;



use Google\Cloud\Storage\Bucket;

class ApplicationState {

	/**
	 * @var DiscordApi
	 */
	var $discord;

    /**
     * @var Bucket
     */
    var $bucket;

	public function __construct( DiscordApi $discordApi , Bucket $bucket) {
		$this->discord = $discordApi;
		$this->bucket = $bucket;
	}

}