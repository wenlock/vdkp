<?php

namespace VkpNinja;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Query\QueryBuilder;
use DOMDocument;
use PDO;
use PDOException;
use vDKP\Import\DKPStringImport;

class Db {

    /**
     * @return PDO
     */
    static function getPdo() {
        static $pdo;
        if (is_null($pdo)) {
            try {
                $pdo = new PDO(
                    getenv("MYSQL_DSN"),
                    getenv("MYSQL_USER"),
                    getenv("MYSQL_PASSWORD"),
                    [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]
                );
            } catch (PDOException $e) {
                /**
                 * To hide username and password from logs.
                 */
                throw new PDOException($e->getMessage());
            }

        }
        return $pdo;
    }

    static function getDbName() {
        $parts = Util::dsnToObj(getenv("MYSQL_DSN"));
        return $parts["dbname"];
    }

    static function getQueryBuilder() {
        static $connection;
        if (is_null($connection)) {
            $connection = DriverManager::getConnection([
                "pdo" => self::getPdo()
            ], new Configuration());
        }
        return new QueryBuilder($connection);
    }

    static function saveDkpString($dkpString) {
        $import = new DKPStringImport($dkpString);
        $events = $import->getAllEvents();
        if (count($events)) {
            $xml = simplexml_load_string($dkpString);
            $domxml = new DOMDocument('1.0');
            $domxml->preserveWhiteSpace = false;
            $domxml->formatOutput = true;
            $domxml->loadXML($xml->asXML());
            $content = $domxml->saveXML();
            $builder = self::getQueryBuilder();
            $result = $builder->insert("uploads")->values([
                "guild_id"=>123,
                "dkpstring" =>$builder->createNamedParameter($content),
                "checksum" => $builder->createNamedParameter(md5($content))
            ])->execute();
            if($result){
                return $builder->getConnection()->lastInsertId();
            }
        }
        return false;
    }
}