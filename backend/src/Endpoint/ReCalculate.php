<?php

namespace VkpNinja\Endpoint;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use vDKP\Export\CsvExport;
use vDKP\Utils;
use VkpNinja\AbstractEndpoint;
use VkpNinja\Calculate;
use VkpNinja\Respond;
use VkpNinja\StorageStructure;

class ReCalculate extends AbstractEndpoint {

    const METHOD = "GET";
    const PATTERN = "/api/guild/{guildId}/recalculate";

    public function __invoke(Request $request, Response $response, $args) {
        $guildId = $args["guildId"];
        $state = Calculate::calc($this->state->bucket, $guildId);
        $allDkp = $state->getAllDKPByPlayer();
        $dkpArray = [];
        foreach ($allDkp as $player=>$dkp){
            $dkpArray[] = [
              "player"=>  $player,
                "points"=>  $dkp,
            ];
        }
        $this->state->bucket->upload(Utils::toCsv($dkpArray), [
            'name' => StorageStructure::results($guildId,"standings.csv")
        ]);
        $this->state->bucket->upload(Utils::toCsv($dkpArray), [
            'name' => StorageStructure::results($guildId,"standings.txt")
        ]);
        $this->state->bucket->upload(Utils::toJson($dkpArray), [
            'name' => StorageStructure::results($guildId,"standings.json")
        ]);

        $transactions = $state->getTransactionsAsArray();
        $this->state->bucket->upload(Utils::toJson($transactions), [
            'name' => StorageStructure::results($guildId,"transactions.csv")
        ]);
        return Respond::json($response, $dkpArray);
    }
}