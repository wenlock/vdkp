<?php


namespace VkpNinja\Endpoint;


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use VkpNinja\AbstractEndpoint;
use VkpNinja\DiscordPermissions;
use VkpNinja\Respond;

class CurrentUserGuilds extends AbstractEndpoint {
	public function __invoke( Request $request, Response $response ) {
		$guilds = $this->state->discord->getGuilds( [
			    "canManageGuild" => DiscordPermissions::MANAGE_GUILD
        ] );
		return Respond::json( $response, $guilds );
	}
}