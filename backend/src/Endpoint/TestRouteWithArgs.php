<?php

namespace VkpNinja\Endpoint;

use cebe\openapi\spec\Operation;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SlimSwagger\OpenapiEndpointInterface;
use SlimSwagger\OpenapiOperation;
use VkpNinja\AbstractEndpoint;
use VkpNinja\Respond;

class TestRouteWithArgs extends AbstractEndpoint implements OpenapiEndpointInterface {

    public function __invoke(Request $request, Response $response, $args) {
        return Respond::json($response, []);

    }

    public function swagger(OpenapiOperation $operation): OpenapiOperation {
        return $operation;
    }
}
