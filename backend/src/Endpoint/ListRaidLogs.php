<?php

namespace VkpNinja\Endpoint;

use Google\Cloud\Storage\StorageObject;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use VkpNinja\AbstractEndpoint;
use VkpNinja\Respond;
use VkpNinja\Storage;
use VkpNinja\StorageStructure;
use VkpNinja\Util;

class ListRaidLogs extends AbstractEndpoint {

    const METHOD = "GET";
    const PATTERN = "/api/guild/{guildId}/raidlogs";

    public function __invoke(Request $request, Response $response, $args) {
        $list = Storage::getRaidLogs($this->state->bucket, $args["guildId"]);
        return Respond::json($response, $list);
    }
}