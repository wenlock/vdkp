<?php


namespace VkpNinja\Endpoint;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use VkpNinja\AbstractEndpoint;

class Hello extends AbstractEndpoint {
	public function __invoke(Request $request, Response $response) {
		$response->getBody()->write( "Hello, Vendetta Ninja	&#x2764;" );

		return $response;
	}
}