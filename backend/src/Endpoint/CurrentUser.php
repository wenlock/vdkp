<?php


namespace VkpNinja\Endpoint;


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use VkpNinja\AbstractEndpoint;
use VkpNinja\Respond;

class CurrentUser extends AbstractEndpoint {
	public function __invoke( Request $request, Response $response ) {
		return Respond::json( $response, $this->state->discord->getUser() );
	}
}