<?php

namespace VkpNinja\Endpoint;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SlimSwagger\OpenapiEndpointInterface;
use SlimSwagger\OpenapiOperation;
use vDKP\Import\DKPStringImport;
use VkpNinja\AbstractEndpoint;
use VkpNinja\Respond;
use VkpNinja\StorageStructure;

use VkpNinja\Util;

class SaveRaidLog extends AbstractEndpoint implements OpenapiEndpointInterface {

    const METHOD = "POST";
    const PATTERN = "/api/guild/{guildId}/raidlogs";

    public function swagger(OpenapiOperation $operation): OpenapiOperation {
        $operation->summary = "My summart";
        $operation->description = "My operation";
        return $operation;
    }

    public function __invoke(Request $request, Response $response, $args=[]) {
        $guildId = $args["guildId"];
        $dkpString = $request->getBody()->getContents();
        $import = new DKPStringImport($dkpString);
        if (count($import->getPlayers()) > 0) {
            $fileName = "";
            if (count($import->zones)) {
                $zone = $import->zones[0];
                $fileParts = [
                    Util::compactTime($zone["enter"]),
                    Util::slugify($zone["name"])
                ];
                $fileName = join("_", $fileParts);
            }
            $objectName = StorageStructure::raidlogs($guildId, "$fileName.xml");
            $this->state->bucket->upload($dkpString, [
                'name' => $objectName
            ]);
            return (new ListRaidLogs($this->state))($request, $response, $args);
        } else {
            return Respond::error($response, 400);
        }

    }


}