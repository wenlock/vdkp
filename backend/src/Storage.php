<?php

namespace VkpNinja;

use Google\Cloud\Storage\Bucket;
use Google\Cloud\Storage\StorageObject;

class Storage {

    public static function getRaidLogs(Bucket $bucket, $guildId) {
        $options = ['prefix' => StorageStructure::raidlogs($guildId)];
        $list = [];
        $bucketName = $bucket->name();
        foreach ($bucket->objects($options) as $object) {
            $info = $object->info();
            /* @var StorageObject $object */
            $list[] = [
                "filename" => basename($object->name()),
                "url" => Util::storageUrl($bucketName, $object->name()),
                "size" => $info["size"],
                "created" => $info["timeCreated"],
                "updated" => $info["updated"],
            ];
        }
        return $list;
    }
}