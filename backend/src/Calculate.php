<?php

namespace VkpNinja;

use vDKP\Utils;

class Calculate {

    public static function calc($bucket, $guildId) {
        $engine = require_once ROOT_DIR . DS . "engine" . DS . "vendetta" . DS . "engine.php";
        $raidLogs = Storage::getRaidLogs($bucket, $guildId);
        $eventFiles = [];
        foreach ($raidLogs as $raidlog) {
            $eventFiles[] = $raidlog["url"];
        }
        return Utils::loadDkpFiles($engine, $eventFiles);
    }
}