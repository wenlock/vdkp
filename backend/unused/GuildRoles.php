<?php


namespace VkpNinja\Endpoint;


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use VkpNinja\AbstractEndpoint;
use VkpNinja\Respond;

class GuildRoles extends AbstractEndpoint {
	const ROUTE = "/api/guild/{guildId}/roles";

	public function __invoke( Request $request, Response $response, $args ) {
		return Respond::json( $response, $this->state->discord->getGuildRoles( $args["guildId"] ) );
	}
}