<?php


namespace VkpNinja;


class Hateoas {
	public static function addLink( $obj, $route, $mapping = [] ) {
		if ( is_null( $obj["links"] ) ) {
			$obj["links"] = [];
		}
		$search  = array_values( $mapping );
		$replace = [];
		foreach ( array_keys( $mapping ) as $key ) {
			$replace[] = $obj[ $key ];
		}
		$path           = str_replace( $search, $replace, $route );
		$obj["links"][] = [
			"href" => (string) Util::getFullUri( $path )
		];

		return $obj;
	}
}