# vdkp

what: check if any new players are added.
data: playersAdded (name=>date)
data: playerBossKills(name)

event: BOSSKILL
1. Award Signon fee
2. Award Boss kill DKP
3. Deduct loot DKP
4. Apply Bonus Rules

event: RAID

System is filebased

## Server
* `vkp.ninja/` - Choose guild
* `vkp.ninja/guild/530476227723657226` - Guildfront with options
  * Standings on first page
* `vkp.ninja/guild/530476227723657226` - Guildfront with standing
* `vkp.ninja/guild/530476227723657226/import` (if you can manage guild)
* `vkp.ninja/guild/530476227723657226/logs`
* `vkp.ninja/guild/530476227723657226/browse`
* `vkp.ninja/guild/530476227723657226/player/wenlock`
* `vkp.ninja/guild/530476227723657226/items`

### Cloud Storage
* `gs://bucket/530476227723657226/events/2019-12-12.xml`
* `gs://bucket/530476227723657226/raw-uploads/5e63f7b9af65c04d8ed4df2da884090d.xml`
* `gs://bucket/530476227723657226/results/transactions.csv`
* `gs://bucket/530476227723657226/results/standing.json`
* `gs://bucket/530476227723657226/results/items.json`
* `gs://bucket/530476227723657226/player/wenlock.json`
